<?php if( $title){ ?>
<h3 class="widget-title"><span><?php echo trim($title); ?></span></h3>
<?php } ?>
<div class="contact-info">
<?php
$params = array(
            'title' => esc_html__('Title', 'evolution'), 
            'description' => esc_html__('Description', 'evolution'), 
            'company' => esc_html__('Company', 'evolution'), 
            'country' => esc_html__('Country', 'evolution'), 
            'locality' => esc_html__('Locality', 'evolution'),
            'region' => esc_html__('Region', 'evolution'),
            'street' => esc_html__('Street', 'evolution'),
            'phone' => esc_html__('Phone', 'evolution'),
            'mobile' => esc_html__('Mobile', 'evolution'),
            'fax' => esc_html__('Fax', 'evolution'),
            'skype' => esc_html__('Skype', 'evolution'),
            'email-address' => esc_html__('Email Address', 'evolution'),
            'email' => esc_html__('Email', 'evolution'),
            'working-hours' => esc_html__('Working Hours', 'evolution'),
            'working-days' => esc_html__('Working Days', 'evolution'),
            'website-url' => esc_html__('Website URL', 'evolution'),
            'website' => esc_html__('Website', 'evolution')
);

foreach ($params as $key => $value) :
    if ($instance[$key]) : 
        switch ($key) { 
            case 'working-days':
                if(isset($instance[$key.'_class']) && !empty($instance[$key.'_class'])): ?>
                        <p class="<?php echo esc_attr( $key ) ?>"><div class="contact-icon"><i class="<?php echo esc_attr( $instance[$key.'_class'] ); ?>" ></i></div><?php echo esc_html( $value ) ?>: <?php echo esc_html( $instance[$key] ); ?></p>
                <?php else: ?>
                <p class="<?php echo esc_attr( $key ) ?>"><?php echo esc_html( $value ); ?>: <?php echo esc_html( $instance[$key] ); ?></p>
                <?php endif;
                break;
            case 'skype':
                if(isset($instance[$key.'_class']) && !empty($instance[$key.'_class'])): ?>
                    <p class="<?php echo esc_attr( $key ) ?>"><div class="contact-icon"><i class="<?php echo esc_attr( $instance[$key.'_class'] ); ?>" ></i></div><?php echo esc_html( $value ) ?>: <?php echo esc_html( $instance[$key] ); ?></p>
                <?php else: ?>
                <p class="<?php echo esc_attr( $key ) ?>"><?php echo esc_html( $value ) ?>: <?php echo esc_html( $instance[$key] ); ?></p>
                <?php endif;
                break;
            case 'title':
            case 'website-url':
                break;
           
            case 'website':
                if(isset($instance[$key.'_class']) && !empty($instance[$key.'_class'])): ?>
                    <p class="<?php echo esc_attr( $key ) ?>"><div class="contact-icon"><i class="<?php echo esc_attr( $instance[$key.'_class'] ); ?>" ></i></div><?php echo esc_html( $value ) ?>: <a href="<?php echo esc_url($instance['website-url']); ?>"><?php if($instance[$key]) { echo esc_html( $instance[$key] ); } else { echo esc_html( $instance[$key] ); } ?></a></p>
                <?php else: ?>
                    <p class="<?php echo esc_attr( $key ) ?>"><?php echo esc_html( $value ) ?> <a href="<?php echo esc_url($instance['website-url']); ?>"><?php if($instance[$key]) { echo esc_html( $instance[$key] ); } else { echo esc_html( $instance[$key] ); } ?></a></p>
                <?php endif;
            break;

            case 'email-address':
                ?>
                    <div class="<?php echo esc_attr( $key ) ?>">
                        <p class="desc"><?php echo esc_html( $value ) ?> </p>
                        <a href="<?php echo esc_url($instance['email-address']); ?>"><?php if($instance[$key]) { echo esc_html( $instance[$key] ); } else { echo esc_html( $instance[$key] ); } ?></a>
                    </div>
                <?php 
                break;

            case 'company':
                ?>
                    <div class="<?php echo esc_attr( $key ) ?>">
                        <p class="desc"><?php echo esc_html__('head Office', "evolution") ?> </p>
                        <a href="<?php echo esc_url($instance['company']); ?>"><?php if($instance[$key]) { echo esc_html( $instance[$key] ); } else { echo esc_html( $instance[$key] ); } ?></a>
                    </div>
                <?php 
                break;

            case 'phone':
                ?>  
                 <div class="phone-number">
                <?php if(isset($instance[$key.'_class']) && !empty($instance[$key.'_class'])): ?>
                    <i class="<?php echo esc_attr( $instance[$key.'_class'] ); ?>" ></i>
                    <span><?php echo esc_html__('Call support free:', "evolution"); ?></span>
                    <p><?php if($instance[$key]) { echo esc_html( $instance[$key] ); } else { echo esc_html( $instance[$key] ); } ?></p>
                <?php else: ?>
                    <span><?php echo esc_html__('Call support free:', "evolution"); ?></span>
                    <p><?php if($instance[$key]) { echo esc_html( $instance[$key] ); } else { echo esc_html( $instance[$key] ); } ?></p>
                <?php endif; ?>
                </div>
                
                
                <?php 
                break;

            case 'email':
                ?> 
                <div class="contact-email">
                <?php
                if(isset($instance[$key.'_class']) && !empty($instance[$key.'_class'])): ?>
                     <i class="<?php echo esc_attr( $instance[$key.'_class'] ); ?>" ></i><span><?php echo esc_html__('Email us:', "evolution"); ?></span> <a href="mailto:<?php echo sanitize_email( $instance['email-address'] ); ?>"><?php if($instance[$key]) { echo esc_html( $instance[$key] ); } ?></a></p>
                <?php else: ?>
                    <p class="<?php echo esc_attr( $key ) ?>"><span><?php echo esc_html__('Email us:', "evolution"); ?></span> <a href="mailto:<?php echo sanitize_email( $instance['email-address'] ); ?>"><?php if($instance[$key]) { echo esc_html( $instance[$key] ); } else { echo esc_html( $instance[$key] ); } ?></a></p>
                <?php endif; ?>

                </div>
                <?php
                break;
            case 'working-hours':
                ?>
                <div class="working-hours">
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <?php if(isset($instance[$key.'_class']) && !empty($instance[$key.'_class'])): ?>
                    
                    <?php echo esc_html( $value ); ?>: <?php echo esc_html( $instance[$key] ); ?></p>
            <?php else: ?>
                <span><?php echo esc_html__('Call support free:', "evolution"); ?></span> 
                <p><?php if($instance[$key]) { echo esc_html( $instance[$key] ); } else { echo esc_html( $instance[$key] ); } ?></p>
                <?php endif;?>
                </div>
             <?php
             break;
            case 'mobile':
                ?>
                    <p><?php if($instance[$key]) { echo esc_html( $instance[$key] ); } else { echo esc_html( $instance[$key] ); } ?></p>
                    </div>
                <?php 
                break;

            default: ?>
                <p class="<?php echo esc_attr( $key ) ?>"><?php echo esc_html( $instance[$key] ); ?></p>
    <?php }
    endif;
endforeach; ?>
</div>