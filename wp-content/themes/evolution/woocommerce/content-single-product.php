<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version       3.3.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<?php
$layouts  = isset($_GET['layout']) ? $_GET['layout'] : evolution_fnc_theme_options('woocommerce-single-content-layout','1');  

if(!empty($layouts) && is_file(get_template_directory().'/woocommerce/single-product/layouts/layout_'.$layouts.'.php')){
	wc_get_template_part( 'woocommerce/single-product/layouts/layout_'.$layouts);
}else{
	wc_get_template_part( 'woocommerce/single-product/layouts/layout_3');
}
?>
<?php do_action( 'woocommerce_after_single_product' ); ?>
