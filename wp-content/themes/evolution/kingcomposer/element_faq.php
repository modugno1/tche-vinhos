<?php

$atts = array_merge( array( 'title' => '','box_wrap_class' => '', ), $atts); 
extract( $atts );
$_id = time().rand();
$columns = 1;
?>
<div class="kc-element-faq-wrapper <?php echo esc_attr($box_wrap_class) ?>">
  <div class="col-md-3 col-sm-3 col-xs-12 kc-element-fag-title">
    <h3><?php echo esc_html( $title ); ?></h3>
  </div>
  <div class="col-md-9 col-sm-9 col-xs-12 kc-element-faq">
      <?php foreach( $options as $option ):   ?>
        <div class="entry-faq">

          <h5><?php echo trim($option->question); ?></h4>
          <p class="faq-description">
              <?php echo trim($option->answer); ?>
          </p>

        </div>  
      <?php endforeach; ?> 
    
  </div>
</div>


