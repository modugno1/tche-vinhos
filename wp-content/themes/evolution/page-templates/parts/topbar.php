
<div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 info">
                
                <?php if( evolution_fnc_theme_options('phone_number') ):  ?>
                    <div class="telephone before">

                        <span class="space-right-5"> <i class="fa fa-phone-square" aria-hidden="true"></i> <?php esc_html_e('Call support free:', 'evolution'); ?></span>
                        
                        <span class="phone-number"><?php echo evolution_fnc_theme_options('phone_number', '1800 - 123 456 78'); ?></span>
                            
                    </div>
                <?php endif; ?>

                <?php if( evolution_fnc_theme_options('topbar_email') ):  ?>
                    <div class="email">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <?php echo evolution_fnc_theme_options('topbar_email', 'info@company.com'); ?>
                    </div>
                <?php endif; ?>
                              
            </div> 
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                
                <div class="hidden-xs hidden-sm">
                    
                    <ul class="list-style  pull-right">
                        

                        <?php if( !is_user_logged_in() ){ ?>
                            <?php
                            do_action( 'opal-account-buttons' );
                        ?>
                        <?php }else{ ?>
                            <?php $current_user = wp_get_current_user(); ?>
                          <li>  <span class="hidden-xs"><?php esc_html_e('Welcome ','evolution'); ?><?php echo esc_html( $current_user->display_name); ?> !</span></li>
                        <?php } ?>
                        <li></li>
                    </ul>
                    
                </div>
            </div>                                 
        </div>
    </div> 