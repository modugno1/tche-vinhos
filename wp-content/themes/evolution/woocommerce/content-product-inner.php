<?php 
$product = wc_get_product();
$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($product->get_id() ), 'blog-thumbnails' );
$term_list = wp_get_post_terms($product->get_id(),'product_cat');

?>
<div class="product-block" data-product-id="<?php echo esc_attr($product->get_id()); ?>">
    <div class="meta">
        <?php if($term_list && isset($term_list[0]) && !is_product_category() ):?>
            <div class="category-name">
                <a href="<?php echo esc_url(get_term_link ($term_list[0]->term_id, 'product_cat'));?>"><?php echo trim($term_list[0]->name); ?></a>
            </div>
        <?php endif; ?>
        <h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

    </div>
    <figure class="image">
        <?php woocommerce_show_product_loop_sale_flash(); ?>
            <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>" class="product-image">
            <?php
                /**
                * woocommerce_before_shop_loop_item_title hook
                *
                * @hooked woocommerce_show_product_loop_sale_flash - 10
                * @hooked woocommerce_template_loop_product_thumbnail - 10
                */
                remove_action('woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash', 10);
                do_action( 'woocommerce_before_shop_loop_item_title' );
            ?>
        </a>
        <div class="button-action clearfix"> 
            <div class="button-groups">

                <?php
                    if( class_exists( 'YITH_WCWL' ) ) {
                        echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
                    }
                ?>

                <?php if( class_exists( 'YITH_Woocompare' ) ) { ?>
                    <?php
                        $action_add = 'yith-woocompare-add-product';
                        $url_args = array(
                            'action' => $action_add,
                            'id' => $product->get_id()
                        );
                    ?>
                    <div class="yith-compare">
                        <a title="<?php esc_html_e( 'Add to compare', 'evolution' ); ?>" href="<?php echo wp_nonce_url( add_query_arg( $url_args ), $action_add ); ?>" class="compare" data-product_id="<?php echo esc_attr($product->get_id()); ?>">
                            <em class="fa fa-refresh"></em>
                        </a>
                    </div>
                <?php } ?>

                <?php if(evolution_fnc_theme_options('is-quickview', true)){ ?>
                    <div class="quick-view hidden-xs hidden-sm">
                        <a title="<?php esc_html_e( 'Quick view', 'evolution' ); ?>" href="#" class="quickview" data-productslug="<?php echo trim($product->get_slug()); ?>" data-toggle="modal" data-target="#opal-quickview-modal">
                           <i class="fa fa-eye"> </i><span><?php esc_html_e( 'Quick view', 'evolution' ); ?></span>
                        </a>
                    </div>
                <?php } ?> 

            </div>
        </div>        
    </figure>
    

    <div class="caption">
        
        
        <?php
                do_action( 'woocommerce_after_shop_loop_item_title');
            ?>

        <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
        <?php
            $action_add = 'yith-woocompare-add-product';
            $url_args = array(
                'action' => $action_add,
                'id' => $product->get_id()
            );
        ?>

    </div>

    
</div>
