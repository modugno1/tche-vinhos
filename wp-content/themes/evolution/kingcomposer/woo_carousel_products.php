<?php 
$type = 'recent_products';
$number_post = 10;
$category = '';
extract( $atts );
$_id = time().rand();

if( isset($woocategory) && !empty($woocategory) ){
	$category = array_keys( wpopal_themer_autocomplete_options_helper($woocategory) );
} 
$loop = wpopal_themer_woocommerce_query( $type, $number_post , $category  );


?>
<div class="products-collection product-carousel-<?php echo esc_attr($style); ?> owl-carousel-play woocommerce" id="postcarousel-<?php echo esc_attr($_id); ?>" data-ride="carousel">
	<?php
		if ($style=='grid') {
			$style = 'inner';
		}elseif ($style=='List-v2') {
			$style = 'list-v2';
		}else {
			$style = 'list';
		}
	  if( $loop->post_count > $columns ) { ?>

	<div class="carousel-controls carousel-controls-v2 carousel-hidden hidden-xs">
		<a href="#postcarousel-<?php echo esc_attr($_id); ?>" data-slide="prev" class="left carousel-control  carousel-md">
			<span class="fa fa-angle-left"></span>
		</a>
		<a href="#postcarousel-<?php echo esc_attr($_id); ?>" data-slide="next" class="right carousel-control  carousel-md">
			<span class="fa fa-angle-right"></span>
		</a>
	</div>
	<?php } ?>
	<div class="owl-carousel" data-slide="<?php echo esc_attr($columns); ?>" data-jumpto="1" data-singleItem="true" data-navigation="false" data-pagination="false">
	<?php $_count =0; while ( $loop->have_posts() ) : $loop->the_post();  ?>
			<?php if($_count%$rows_count == 0){
							
							echo '<div class="item">';
			} ?>
			<div  <?php post_class( 'product-carousel-item' ); ?>><?php wc_get_template_part( 'content', 'product-'.$style ); ?>
				
			</div>
			<?php if($_count%$rows_count == $rows_count-1 || $_count==$loop->post_count -1){
							echo '</div>';
						}
						$_count++; ?>
	<?php endwhile; ?>
	</div>
</div>

<?php wp_reset_postdata(); ?>