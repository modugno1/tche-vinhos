<?php

/**
 * Load woocommerce styles follow theme skin actived
 *
 * @static
 * @access public
 * @since Wpopal_Themer 1.0
 */

/// put your code here 

add_action('init', 'evolution_element_evolution_map', 99 );

function evolution_element_evolution_map(){
	global $kc;
	$maps = array();
	$maps['element_faq'] =array(
		'name' =>esc_html__('FAQ', 'evolution'),
		'icon' => 'sl-paper-plane',
		'class' => '',
		'description' => '',
		'category' =>esc_html__('Elements', 'evolution'),
		'params' => array(
			array(
				'type' => 'textfield',
				'label' =>esc_html__('Title', 'evolution'),
				'name' => 'title',
				'value' => '',
				'admin_label' => true
			),
			array(
				'type'			=> 'group',
				'label'			=>esc_html__('Options', 'evolution'),
				'name'			=> 'options',
				'description'	=>esc_html__( 'Repeat this fields with each item created, Each item corresponding element.', 'evolution' ),
				'options'		=> array('add_text' =>esc_html__('Add new item', 'evolution')),
				'value' => '',
				'params' => array(
					array(
						"type"        => "textfield",
						"label"       => esc_html__("Question", 'evolution'),
						"name"        => "question",
						"value"       => '',
						"admin_label" => true
					),
					array(
						"type"        => "textarea",
						"label"       => esc_html__("Answer", 'evolution'),
						"name"        => "answer",
						"value"       => '',
						"admin_label" => true
					),
				),
			),
			array(
					'name'	        => 'box_wrap_class',
					'label'	        => 'Wrapper class name',
					'type'	        => 'text',
					'description'   => esc_html__('Enter class name for wrapper', 'evolution'),
			),
		)
	);
	$maps['element_history_about'] =  array(
     
        'name' => esc_html__('History About', 'evolution'),
        'description' => esc_html__('', 'evolution'),
        'icon' => 'kc-icon-accordion',
        'category' => 'Content',
        'css_box' => true,
        'params' => array(
            array(
                'type'            => 'group',
                'label'            => esc_html__('Options', 'evolution'),
                'name'            => 'options',
                'description'    => esc_html__( 'Repeat this fields with each item created, Each item corresponding processbar element.', 'evolution' ),
                'options'        => array('add_text' => esc_html__('Add new question', 'evolution')),
                'params' => array(   
           			array(
                        'type' => 'text',
                        'label' => esc_html__( 'Title', 'evolution' ),
                        'name' => 'title',
                        'description' => esc_html__( 'Enter text used as title of bar.', 'evolution' ),
                        'admin_label' => false,
                  ),
                  array(
                        'type' => 'textarea',
                        'label' => esc_html__( 'Content', 'evolution' ),
                        'name' => 'content',
                        'description' => esc_html__( 'Enter text used as content of bar.', 'evolution' ),
                        'admin_label' => false,
                    )
                ),
            )
        )
     
    );
	$option_menu  = array(); 
	if( is_admin() ){
	$menus = wp_get_nav_menus( array( 'orderby' => 'name' ) );
	    $option_menu = array('---Select Menu---'=>'');
	    foreach ($menus as $menu) {
	    
	$option_menu[$menu->slug]=$menu->name;
	    }
	}    


	//Add element Vertical menu
	$maps['element_menuvertical'] =array(
		'name' =>esc_html__("Menu Vertical", 'evolution'),
		'icon' => 'sl-paper-plane',
		"class" => "",
		"description" => 'Set up menu vertical for page',
		"category" =>esc_html__('Elements', 'evolution'),
		"params" => array(
			array(
				"type" => "textfield",
				"label" =>esc_html__("Title", 'evolution'),
				"name" => "title",
				"value" => '',
			),

			array(
				'type' => 'select',
				'label' =>esc_html__( 'Menu select', 'evolution' ),
				'name' => 'menu',
				"options" => $option_menu,
				'description' =>esc_html__('Select menu.', 'evolution'),
			),

			array(
				"type" => "select",
				"label" => esc_html__("Position", 'evolution'),
				'name' => 'position',
				"options" => array(
					'left'=>'left',	
					'right'=>'right'
				),
				"description" => esc_html__("Postion Menu Vertical.", 'evolution')
			),
			array(
				"type" => "textfield",
				"label" => esc_html__("Extra class name", 'evolution'),
				"name" => "el_class",
				"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'evolution')
			),
		)
	);

	// Featured Box
	

		// ourservice
	$maps['element_ourservice'] =  array(
			'name' => esc_html__( 'Ourservice', 'evolution' ),
			'title' => 'Ourservice',
			'icon' => 'kc-icon-icon',
			'category' => 'Elements',
			'wrapper_class' => 'clearfix',
			'description' => esc_html__( 'Ourservice.', 'evolution' ),
			'params' => array(
				
				array(
						'type'			=> 'dropdown',
						'label'			=> __( 'Style', 'evolution' ),
						'name'			=> 'style',
						'description'	=> __( 'Select the style of progress bars.', 'evolution' ),
						'options'		=> array(
							'1' => esc_html__(' Style 1 (Ourservice Vertical)', 'evolution'),
							'2' => esc_html__(' Style 2 (Ourservice Horizontal)', 'evolution'),
						),
						'admin_label'	=> true,
				),

			   array(
					'type'			=> 'group',
					'label'			=> esc_html__(' Options', 'evolution'),
					'name'			=> 'options',
					'description'	=> __( 'Repeat this fields with each item created, Each item corresponding processbar element.', 'evolution' ),
					'options'		=> array('add_text' => esc_html__(' Add new items Ourservice', 'evolution')),

					'value' => json_encode(array(
						"1" => array(
							"label" => "Select Item",
							"textarea" => "",
						),
						"2" => array(
							"label" => "Payment",
							"textarea" => "",
						),
						"3" => array(
							"label" => "Delivery",
							"textarea" => "",
						),
						"4" => array(
							"label" => "Got your item",
							"textarea" => "",
						)
					) ) ,
					'params' => array(
						array(
							'type' => 'text',
							'label' => __( 'Label', 'evolution' ),
							'name' => 'label',
							'description' => __( 'Enter text used as title of the bar.', 'evolution' ),
							'admin_label' => true,
						),
						array(
							'name'	      => 'info',
							'label'	      => esc_html__('Information', 'evolution'),
							'type'	      => 'textarea',
							'admin_label' => true,
							'description' => esc_html__('', 'evolution')
						),
					),
				),
			   array(
					"type" => "textfield",
					"label" => esc_html__("Extra class name", 'evolution'),
					"name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'evolution')
				),
             // end params
			)
		);
	$kc->add_map_param(
			'element_blog_posts',
			array(
				'type'			=> 'multiple',
				'label'			=> esc_html__( 'Select Categories', 'evolution' ),
				'name'			=> 'tax_term',
				'options'		=> kc_tools::get_terms( 'category', 'slug' ),
				'height'		=> '120px',
				'admin_label'	=> true,
				'description'	=> esc_html__( 'Select category which you chosen for posts ( hold ctrl or shift to select multiple )', 'evolution' )
			)
		, 1);
	
    
    
    
    $maps = apply_filters( 'evolution_element_kingcomposer_map', $maps ); 
   $kc->add_map( $maps );
} // end class

// add map param
add_action('init', 'evolution_kc_single_image_add_map_param', 99 );
function evolution_kc_single_image_add_map_param(){
    global $kc;
    $kc->add_map_param(
     'element_blog_posts',
        array(
		    'name' => 'rows_count',
		    'label' => 'Rows column',
		    'type' => 'number_slider',
		    'options' => array(
				'min' => 1,
				'max' => 4,
				'unit' => '',
				'show_input' => true
			),

			"admin_label" => true,
			'description' => 'Display number rows of post'
		   )
 	, 2);	
}



//add element Products
if( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
	add_action('init', 'evolution_kc_woo_carousel_products_add_map_param', 99 );
}
function evolution_kc_woo_carousel_products_add_map_param(){
	global $kc;
	$product_layouts = array(
    	'grid' 	=> esc_html__('Grid', 'evolution') ,
    	'list' => esc_html__('List', 'evolution'),
    	'List-v2' => esc_html__('list 2', 'evolution')
	);
	$kc->add_map_param(
		'woo_grid_products',
			array(
				"type" => "select",
				"label" => esc_html__("Style",'evolution'),
				"name" => "style",
				"options" => $product_layouts
			)
		, 3);
	$kc->add_map_param(
		'woo_grid_products',
			array(
            'name' => 'product_style',
            'label' => 'Style Product',
            'type' => 'select',
            'value' => 'style-1',
           	'options' => array(
	        		'style-1'   	=> esc_html__( 'Style 1', 'evolution' ),
	         	'style-2'   	=> esc_html__( 'Style 2', 'evolution' ),
	        	),
            'description' => 'Select style for product'
      	)
		, 10);
	$kc->add_map_param(
		'woo_deal_products',
			array(
				"type" => "select",
				"label" => esc_html__("Style",'evolution'),
				"name" => "style",
				"options" => array(
					'list' => esc_html__('List', 'evolution'),
					'grid' 	=> esc_html__('Grid', 'evolution') ,
				)
			)
		, 2);
	$kc->add_map_param(
		'woo_deal_products',
			array(
				"type" => "select",
				"label" => esc_html__("Layout",'evolution'),
				"name" => "layout",
				"options" => array(
					'carousel' => esc_html__('Carousel', 'evolution'),
					'grid' 	=> esc_html__('Grid', 'evolution') ,
				)
			)
		, 3);
	$kc->add_map_param(
		'woo_deal_products',
			array(
				"type" => "textfield",
				"label" => esc_html__("Extra class name", 'evolution'),
				"name" => "el_class",
				"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'evolution')
			)
		, 10);
	$kc->add_map_param(
		'woo_carousel_products',
			array(
				"type" => "select",
				"label" => esc_html__("Style",'evolution'),
				"name" => "style",
				"options" => $product_layouts
			)
		, 3);
	$kc->add_map_param(
	'woo_carousel_products',
		array(
        'name' => 'rows_count',
        'label' => esc_html__( 'Row Carousel Products' ,'evolution' ),
        'type' => 'number_slider',
        'value' => 1,
        'options' => array(
            'min' => 1,
            'max' => 4,
            'unit' => '',
            'show_input' => true
        ),
        
	        'description' => 'Display row of products'
	    )
	, 6);

		///
	$kc->add_map(
	         array(
	            'woo_deal_products_v2' => array(
	                'name' => 'Deal Products v2',
	                'description' => esc_html__('Display Product Deals in grid', 'evolution'),
	                'icon' => 'sl-paper-plane',
	                'category' => 'Woocommerce',

	                'params' => array(
                        array(
							'type'           => 'autocomplete',
							'label'          => __( 'Select Category', 'evolution' ),
							'name'           => 'woocategory',
							'description'    => __( 'Select Category to display', 'evolution' ),
							'admin_label'    => true,
							'options' => array( 'category_name' => 'product_cat' )

	                    ),
	                    array(
	                        'name' => 'number_post',
	                        'label' => 'Number post show',
	                        'type' => 'number_slider',
	                        'value' => 4,
	                        'admin_label'    => true,
	                        'options' => array(
	                            'min' => 1,
	                            'max' => 8,
	                            'unit' => '',
	                            'show_input' => true
	                        ),
	                        'description' => 'Display number of post'
	                    ),

	                    array(
	                        'name' => 'columns',
	                        'label' => __( 'Grid Column' ,'evolution' ),
	                        'type' => 'number_slider',
	                        'value' => 4,
	                        'options' => array(
	                            'min' => 1,
	                            'max' => 6,
	                            'unit' => '',
	                            'show_input' => true
	                        ),
	                        'description' => 'Display number of post'
	                    ),
	                )
	            )
	        )
	    );
	$kc->add_map(
         array(
            'woo_carousel_products_v2' => array(
                'name' => 'Carousel Products 2',
                'description' => esc_html__('Display Bestseller, Latest, Most Review ... in grid', 'evolution'),
                'icon' => 'sl-paper-plane',
                'category' => 'Woocommerce',

                'params' => array(
                		array(
					        'name' => 'position',
					        'label' => esc_html__( 'Product main position' ,'evolution' ),
					        'type' => 'select',
					        'value' => 'left',
					        'options' => array(
					        		'top'   	=> esc_html__( 'Top', 'evolution' ),
					            'left'   	=> esc_html__( 'Left', 'evolution' ),
					            'right'   	=> esc_html__( 'Right', 'evolution' ),
					        ),
					        'description' => 'Select position for main product'
					    	),
                    	array(
                        'name' => 'type',
                        'label' => 'Product Type',
                        'type' => 'select',
                        'admin_label' => true,
                         'options' => array(  // THIS FIELD REQUIRED THE PARAM OPTIONS
					        'recent_products'   	=> esc_html__( 'Recent Products', 'evolution' ),
					        'sale_products' 		=> esc_html__( 'Sale Products', 'evolution' ),
					        'featured_products' 	=> esc_html__( 'Featured Products', 'evolution' ),
					        'best_selling_products'	=> esc_html__( 'Best Selling Products', 'evolution' ),
					        'products'				=> esc_html__( 'Products', 'evolution' ),
					    	)
                   	),
                     array(
						'type'           => 'autocomplete',
						'label'          => esc_html__( 'Select Category', 'evolution' ),
						'name'           => 'woocategory',
						'description'    => esc_html__( 'Select Category to display', 'evolution' ),
						'admin_label'    => true,
						'options' => array( 'taxonomy' => 'product_cat' )

                    ),

                    array(
                        'name' => 'number_post',
                        'label' => 'Number post show',
                        'type' => 'number_slider',
                        'options' => array(
                            'min' => 1,
                            'max' => 24,
                            'unit' => '',
                            'show_input' => true
                        ),
                        'description' => 'Display number of post'
                    ),

                    array(
                        'name' => 'number_per_post',
                        'label' => 'Number Per page  ',
                        'type' => 'number_slider',
                        'value' => 5,
                        'options' => array(
                            'min' => 1,
                            'max' => 11,
                            'unit' => '',
                            'show_input' => true
                        ),
                        'description' => 'Display the number product show on per page'
                    ),
                    array(
                        'name' => 'style',
                        'label' => 'Style Product',
                        'type' => 'select',
                        'value' => 'style-1',
                       	'options' => array(
					        		'style-1'   	=> esc_html__( '1 column product', 'evolution' ),
					         	'style-2'   	=> esc_html__( '2 column product', 'evolution' ),
					        	),
                        'description' => 'Select style for product'
                     ),
                )
            )
        )
    );
	$kc->add_map(
	      array(
            'woo_shop_category' => array(
                'name' => 'Shop by Category',
                'description' => esc_html__('Display Shop Category', 'evolution'),
                'icon' => 'sl-paper-plane',
                'category' => 'Woocommerce',

                "params" => array(
		    
			    	 		array(
								"type" => "textfield",
								"label" => esc_html__("Title", 'evolution'),
								"name" => "title",
								"value" => 'Shop by Category',
								"admin_label" => true
							),
							array(
						        'name' => 'number_cats',
						        'label' => esc_html__( 'Number categories' ,'evolution' ),
						        'type' => 'number_slider',
						        'value' => 8,
						        'options' => array(
						            'min' => 1,
						            'max' => 20,
						            'unit' => '',
						            'show_input' => true
						        ),
						        'description' => 'Display number of category'
						    ),
							array(
						        'name' => 'columns',
						        'label' => esc_html__( 'Grid Column' ,'evolution' ),
						        'type' => 'number_slider',
						        'value' => 6,
						        'options' => array(
						            'min' => 1,
						            'max' => 6,
						            'unit' => '',
						            'show_input' => true
						        ),
						        'description' => 'Display culumn of category'
						    ),

							array(
								"type"        => "textfield",
								"label"     => esc_html__("Extra class name",'evolution'),
								"name"  => "el_class",
								"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'evolution')
							)
		   		) 
              		 
         ) )
	   );
	
}


