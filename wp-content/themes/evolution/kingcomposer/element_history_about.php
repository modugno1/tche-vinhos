<?php 
  extract( $atts );
?>
<?php if( isset($options) ) : 
?>
<div class="element-history-about"> 
  <?php foreach( $options as $option ):   ?>
    <div class="entry-history-about">

          <h4><?php echo trim($option->title); ?></h4>
          <p class="history-about-description">
            <?php echo trim($option->content); ?>
          </p>

    </div>  
  <?php endforeach; ?> 
</div>  
<?php endif; ?>


