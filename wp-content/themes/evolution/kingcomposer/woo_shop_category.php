<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WpOpal Team <opalwordpress@gmail.com>
 * @copyright  Copyright (C) 2016 http://www.wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */

 
extract( $atts );

$_id = rand();
$_count = 0;

$args = array(
     'taxonomy'     => 'product_cat',
     'number'     => $number_cats,
);
$all_categories = get_categories( $args );
$_totals = count($all_categories);

if($all_categories): ?>
    <div class="opal-category-shop woocommerce <?php echo (($el_class!='')?' '.esc_attr( $el_class ):''); ?>">
        <div class="title"><span><?php echo trim($title);?></span></div>
        <div class="inner nopadding">
            <div class="products-collection owl-carousel-play woocommerce carousel-products" id="postcarousel-<?php echo esc_attr($_id); ?>" data-ride="carousel">
                <?php if( $_totals > $columns ): ?>
                    <div class="carousel-controls  carousel-hidden hidden-xs">
                        <a href="#postcarousel-<?php echo esc_attr($_id); ?>" data-slide="prev" class="left carousel-control carousel-md">
                            <span class="fa fa-chevron-left"></span>
                        </a>
                        <a href="#postcarousel-<?php echo esc_attr($_id); ?>" data-slide="next" class="right carousel-control carousel-md">
                            <span class="fa fa-chevron-right"></span>
                        </a>
                    </div>
                <?php endif; ?>
                <div class="owl-carousel" data-slide="<?php echo esc_attr($columns);?>"  data-singleItem="true" data-navigation="false" data-pagination="false">
                    <?php foreach($all_categories as $_category): ?>
                        <div class="cats-item">
                            <div class="image-category">
                                <a href="<?php echo esc_url( get_term_link( $_category->slug, 'product_cat' ) ); ?>">
                                     <?php 
                                        $thumbnail_id = get_woocommerce_term_meta( $_category->term_id, 'thumbnail_id', true );
                                        $image = wp_get_attachment_url( $thumbnail_id );
                                        if ( $image ) {
                                            echo '<img src="' . esc_url_raw( $image ) . '" alt="'.$_category->name.'" />';
                                        }else{
                                            echo '<img src="'.esc_url( get_template_directory_uri() ) . '/images/cats-placeholder.png'.'" alt="'.$_category->name.'" />';
                                        }
                                    ?>
                                </a>
                               
                            </div>
                            <div class="title-category"><a href="<?php echo esc_url( get_term_link( $_category->slug, 'product_cat' ) ); ?>"><?php echo trim($_category->name); ?></a></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>