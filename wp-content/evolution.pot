#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Opal Theme\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:"
"1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;"
"esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"POT-Creation-Date: 2017-10-18 04:16+0000\n"
"Language: \n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: Loco - https://localise.biz/"

#. Name of the template
msgid "404 Page"
msgstr ""

#: 404.php:32
msgid "Page Not Found !"
msgstr ""

#: 404.php:34
msgid "We are sorry, but something went wrong"
msgstr ""

#: 404.php:38
msgid "Return to homepage"
msgstr ""

#: author.php:40
#, php-format
msgid "All posts by %s"
msgstr ""

#: category.php:36
#, php-format
msgid "Category Archives: %s"
msgstr ""

#: comments.php:21
msgid "1 Comment"
msgstr ""

#: comments.php:21
msgid "% Comments"
msgstr ""

#: comments.php:34
msgid "&larr; Older Comments"
msgstr ""

#: comments.php:35
msgid "Newer Comments &rarr;"
msgstr ""

#: comments.php:40
msgid "Comments are closed."
msgstr ""

#: comments.php:48
msgid "Leave a Comment"
msgstr ""

#: comments.php:50
msgid "Comment:"
msgstr ""

#: comments.php:57
msgid "Name:"
msgstr ""

#: comments.php:61
msgid "Email:"
msgstr ""

#: comments.php:65
msgid "Website:"
msgstr ""

#: comments.php:69
msgid "Post Comment"
msgstr ""

#: comments.php:70
msgid "Your email address will not be published."
msgstr ""

#: content-aside.php:17 content-audio.php:29 content-featured-post.php:28
#: content-gallery.php:41 content-image.php:23 content-link.php:31
#: content-quote.php:22 content-video.php:27
msgctxt "Used between list items, there is a space after the comma."
msgid ", "
msgstr ""

#: content-aside.php:34 content-audio.php:46 content-gallery.php:58
#: content-image.php:39 content-link.php:48 content-quote.php:39
#: content-video.php:44 content.php:30
msgid "by"
msgstr ""

#: content-aside.php:42 content-audio.php:55 content-gallery.php:67
#: content-image.php:49 content-link.php:57 content-quote.php:48
#: content-video.php:53 content.php:44 inc/template-tags.php:234
#, php-format
msgid "Continue reading %s"
msgstr ""

#: content-aside.php:47 content-audio.php:63 content-gallery.php:75
#: content-image.php:57 content-link.php:65 content-page.php:17
#: content-quote.php:56 content-video.php:61 content.php:52 image.php:54
msgid "Pages:"
msgstr ""

#: content-blog.php:28 widgets/latest_posts/default.php:45
msgid "Post by:"
msgstr ""

#: content-blogv1.php:30
msgid "Posted by"
msgstr ""

#: content-none.php:12
msgid "Nothing Found"
msgstr ""

#: content-none.php:18
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: content-none.php:22
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: content-none.php:27
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: content-page.php:23 image.php:34 page-templates/contributors.php:29
msgid "Edit"
msgstr ""

#: functions.php:91
msgid "Main menu"
msgstr ""

#: functions.php:92
msgid "Topbar Menu"
msgstr ""

#: functions.php:177
msgid "MetaBox"
msgstr ""

#: functions.php:183
msgid "WooCommerce"
msgstr ""

#: functions.php:190
msgid "MailChimp"
msgstr ""

#: functions.php:196
msgid "Contact Form 7"
msgstr ""

#: functions.php:202
msgid "King Composer - Page Builder"
msgstr ""

#: functions.php:209 inc/functions-import.php:42
msgid "Revolution Slider"
msgstr ""

#: functions.php:216
msgid "Wpopal Themer For Themes"
msgstr ""

#: functions.php:223
msgid "YITH WooCommerce Wishlist"
msgstr ""

#: functions.php:240
msgid "Sidebar Default"
msgstr ""

#: functions.php:242 functions.php:264 functions.php:275 functions.php:286
#: functions.php:296 functions.php:307 functions.php:318
msgid "Appears on posts and pages in the sidebar."
msgstr ""

#: functions.php:251
msgid "Topbar"
msgstr ""

#: functions.php:253
msgid "Appears in the Topbar section of the site."
msgstr ""

#: functions.php:262
msgid "Contact Header"
msgstr ""

#: functions.php:273
msgid "Vertical Menu"
msgstr ""

#: functions.php:284
msgid "Left Sidebar"
msgstr ""

#: functions.php:294
msgid "Right Sidebar"
msgstr ""

#: functions.php:305
msgid "Blog Left Sidebar"
msgstr ""

#: functions.php:316
msgid "Blog Right Sidebar"
msgstr ""

#: functions.php:327
msgid "Footer 1"
msgstr ""

#: functions.php:329 functions.php:339 functions.php:349 functions.php:359
#: functions.php:369
msgid "Appears in the footer section of the site."
msgstr ""

#: functions.php:337
msgid "Footer 2"
msgstr ""

#: functions.php:347
msgid "Footer 3"
msgstr ""

#: functions.php:357
msgid "Footer 4"
msgstr ""

#: functions.php:367
msgid "Footer 5"
msgstr ""

#: functions.php:379
msgid "Woocommerce Footer Body"
msgstr ""

#: functions.php:381
msgid "Appears in the end of woocommerce footer section of the site."
msgstr ""

#: functions.php:391
msgid "Custom Service"
msgstr ""

#: functions.php:393
msgid "Appears in the header right section of the site."
msgstr ""

#: functions.php:402
msgid "Sidebar Home"
msgstr ""

#: functions.php:404
msgid "Appears in the Home section of the site."
msgstr ""

#: functions.php:430
msgctxt "Poppins font: on or off"
msgid "on"
msgstr ""

#: functions.php:432
msgctxt "Pay fair font: on or off"
msgid "on"
msgstr ""

#: image.php:65
msgid "Previous Image"
msgstr ""

#: image.php:66
msgid "Next Image"
msgstr ""

#: list_comments.php:30 inc/function-post.php:178
#, php-format
msgid "%1$s at %2$s"
msgstr ""

#: list_comments.php:31 inc/function-post.php:179
msgid " - Edit"
msgstr ""

#: list_comments.php:32 inc/function-post.php:180
msgid " - Reply"
msgstr ""

#: list_comments.php:37 inc/function-post.php:185
msgid "Your comment is awaiting moderation."
msgstr ""

#: search.php:20
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: searchform.php:17
msgid "Search..."
msgstr ""

#: sidebar-offcanvas.php:7
msgid "Menu"
msgstr ""

#: sidebar-static-left.php:17
msgid "Please drag and drop widget on this position Static Left"
msgstr ""

#: tag.php:30
#, php-format
msgid "Tag Archives: %s"
msgstr ""

#: taxonomy-post_format.php:30
msgid "Asides"
msgstr ""

#: taxonomy-post_format.php:33
msgid "Images"
msgstr ""

#: taxonomy-post_format.php:36
msgid "Videos"
msgstr ""

#: taxonomy-post_format.php:39
msgid "Audio"
msgstr ""

#: taxonomy-post_format.php:42
msgid "Quotes"
msgstr ""

#: taxonomy-post_format.php:45
msgid "Links"
msgstr ""

#: taxonomy-post_format.php:48
msgid "Galleries"
msgstr ""

#: taxonomy-post_format.php:51
msgid "Archives"
msgstr ""

#: inc/customizer-config.php:23
msgid "General Settings"
msgstr ""

#: inc/customizer-config.php:24
msgid "Website General Settings"
msgstr ""

#: inc/customizer-config.php:38
msgid "Phone Number"
msgstr ""

#: inc/customizer-config.php:55 woocommerce/single-product-reviews.php:123
#: widgets/contact-info/default.php:19
msgid "Email"
msgstr ""

#: inc/customizer-config.php:72
msgid "Enable Title"
msgstr ""

#: inc/customizer-config.php:89
msgid "Enable Banner"
msgstr ""

#: inc/customizer-config.php:105
msgid "Enable title"
msgstr ""

#: inc/customizer-config.php:120
msgid "Product zoom mode"
msgstr ""

#: inc/customizer-config.php:126
msgid "Basic"
msgstr ""

#: inc/customizer-config.php:127
msgid "Inner"
msgstr ""

#: inc/customizer-config.php:145
msgid "Remove Box Shadow"
msgstr ""

#: inc/customizer.php:19
msgid "Colors"
msgstr ""

#: inc/customizer.php:25
msgid "Main Color"
msgstr ""

#: inc/customizer.php:40
msgid "Primary Color"
msgstr ""

#: inc/customizer.php:46
msgid "Header"
msgstr ""

#: inc/customizer.php:61
msgid " Topbar Background"
msgstr ""

#: inc/customizer.php:76
msgid "Topbar color"
msgstr ""

#: inc/customizer.php:91
msgid "Header Background"
msgstr ""

#: inc/customizer.php:106
msgid "Menu color"
msgstr ""

#: inc/customizer.php:121
msgid "Menu Active, Hover color"
msgstr ""

#: inc/customizer.php:127
msgid "Footer"
msgstr ""

#: inc/customizer.php:142
msgid "Footer Copyright Background"
msgstr ""

#: inc/customizer.php:157
msgid "Copyright Color"
msgstr ""

#: inc/function-post.php:101
#, php-format
msgid "%d Article"
msgid_plural "%d Articles"
msgstr[0] ""
msgstr[1] ""

#: inc/function-post.php:207
msgid "Previous"
msgstr ""

#: inc/function-post.php:207
msgid "Next"
msgstr ""

#: inc/function-post.php:215
msgid "Showing the single result"
msgstr ""

#: inc/function-post.php:217
#, php-format
msgid "Showing all %d results"
msgstr ""

#: inc/function-post.php:219
#, php-format
msgctxt "%1$d = first, %2$d = last, %3$d = total"
msgid "Showing %1$d to %2$d of %3$d results"
msgstr ""

#: inc/functions-import.php:37
msgid "All"
msgstr ""

#: inc/functions-import.php:38 inc/vendors/kingcomposer/functions.php:88
msgid "Content"
msgstr ""

#: inc/functions-import.php:39
msgid "Widgets"
msgstr ""

#: inc/functions-import.php:40
msgid "Theme + Page Options"
msgstr ""

#: inc/functions-import.php:41
msgid "Menus"
msgstr ""

#: inc/functions-import.php:43
msgid "VC Templates"
msgstr ""

#: inc/template-tags.php:50
msgid "&larr; Previous"
msgstr ""

#: inc/template-tags.php:51
msgid "Next &rarr;"
msgstr ""

#: inc/template-tags.php:58
msgid "Posts navigation"
msgstr ""

#: inc/template-tags.php:85
msgid "Post navigation"
msgstr ""

#: inc/template-tags.php:89
msgid "Published In"
msgstr ""

#: inc/template-tags.php:91
msgid "Previous Post"
msgstr ""

#: inc/template-tags.php:92
msgid "Next Post"
msgstr ""

#: inc/template-tags.php:109
msgid "Sticky"
msgstr ""

#: inc/template-tags.php:280
msgid "Related posts"
msgstr ""

#: inc/template.php:466
#, php-format
msgid "Page %s"
msgstr ""

#: inc/template.php:560
msgid "Search results for \""
msgstr ""

#: inc/template.php:563
msgid "Posts tagged \""
msgstr ""

#: inc/template.php:568
msgid "Articles posted by "
msgstr ""

#: inc/template.php:571
msgid "Error 404"
msgstr ""

#: inc/template.php:586
#, php-format
msgid "Proudly powered by %s. Developed by %s"
msgstr ""

#: inc/template.php:602
msgid "Search Products"
msgstr ""

#: inc/template.php:610 inc/template.php:638
msgid "Search"
msgstr ""

#: kingcomposer/woo_category_subs.php:67 kingcomposer/woo_category_subs.php:106
msgid "more"
msgstr ""

#: kingcomposer/woo_category_subs.php:67
msgid "view all"
msgstr ""

#: kingcomposer/woo_category_subs.php:106
msgid "View all"
msgstr ""

#: kingcomposer/woo_deal_products.php:60
#: kingcomposer/woo_deal_products_v2.php:60
msgid "previous deal"
msgstr ""

#: kingcomposer/woo_deal_products.php:63
#: kingcomposer/woo_deal_products_v2.php:63
msgid "next deal"
msgstr ""

#: kingcomposer/woo_deal_products.php:112
#: kingcomposer/woo_deal_products.php:220
#: kingcomposer/woo_deal_products_v2.php:115
#: woocommerce/content-product-inner-list.php:41
#: woocommerce/content-product-inner.php:49
#: woocommerce/content-product-list-v2.php:41
msgid "Add to compare"
msgstr ""

#: kingcomposer/woo_deal_products.php:120
#: kingcomposer/woo_deal_products.php:121
#: kingcomposer/woo_deal_products.php:228
#: kingcomposer/woo_deal_products.php:229
#: kingcomposer/woo_deal_products_v2.php:123
#: kingcomposer/woo_deal_products_v2.php:124
#: woocommerce/content-product-inner-list.php:49
#: woocommerce/content-product-inner-list.php:50
#: woocommerce/content-product-inner.php:57
#: woocommerce/content-product-inner.php:58
#: woocommerce/content-product-list-v2.php:49
#: woocommerce/content-product-list-v2.php:50
msgid "Quick view"
msgstr ""

#: kingcomposer/woo_deal_products.php:155
#: kingcomposer/woo_deal_products.php:263
#: kingcomposer/woo_deal_products_v2.php:158
msgid "hurry up! Offer Ends in:"
msgstr ""

#: kingcomposer/woo_deal_products.php:156
#: kingcomposer/woo_deal_products.php:264
#: kingcomposer/woo_deal_products_v2.php:159
msgid "Days"
msgstr ""

#: kingcomposer/woo_deal_products.php:156
#: kingcomposer/woo_deal_products.php:264
#: kingcomposer/woo_deal_products_v2.php:159
msgid "Hours"
msgstr ""

#: kingcomposer/woo_deal_products.php:156
#: kingcomposer/woo_deal_products.php:264
#: kingcomposer/woo_deal_products_v2.php:159
msgid "Mins"
msgstr ""

#: kingcomposer/woo_deal_products.php:156
#: kingcomposer/woo_deal_products.php:264
#: kingcomposer/woo_deal_products_v2.php:159
msgid "Secs"
msgstr ""

#. Name of the template
msgid "Boxed Page"
msgstr ""

#. Name of the template
msgid "Contributor"
msgstr ""

#. Name of the template
msgid "Full Width Page"
msgstr ""

#. Name of the template
msgid "Main Left Sidebar"
msgstr ""

#: woocommerce/add-to-wishlist-button.php:13
msgid "Add to wishlist"
msgstr ""

#: woocommerce/single-product-reviews.php:40
msgid "Customers review"
msgstr ""

#: woocommerce/single-product-reviews.php:45
#, php-format
msgid "%s ratings"
msgstr ""

#: woocommerce/single-product-reviews.php:51
msgid "Star"
msgstr ""

#: woocommerce/single-product-reviews.php:66
msgid "Rate it!"
msgstr ""

#: woocommerce/single-product-reviews.php:67
msgid ""
"Only logged in customers who have purchased this product may leave a review."
msgstr ""

#: woocommerce/single-product-reviews.php:68
msgid "Write A Review"
msgstr ""

#: woocommerce/single-product-reviews.php:79
#, php-format
msgid "%1$s review for %2$s"
msgid_plural "%1$s reviews for %2$s"
msgstr[0] ""
msgstr[1] ""

#: woocommerce/single-product-reviews.php:81
msgid "Reviews"
msgstr ""

#: woocommerce/single-product-reviews.php:102
msgid "There are no reviews yet."
msgstr ""

#: woocommerce/single-product-reviews.php:115
msgid "Add a review"
msgstr ""

#: woocommerce/single-product-reviews.php:115
msgid "Be the first to review"
msgstr ""

#: woocommerce/single-product-reviews.php:116
#, php-format
msgid "Leave a Reply to %s"
msgstr ""

#: woocommerce/single-product-reviews.php:121
msgid "Name"
msgstr ""

#: woocommerce/single-product-reviews.php:126
msgid "Submit"
msgstr ""

#: woocommerce/single-product-reviews.php:137
msgid "Your Rating"
msgstr ""

#: woocommerce/single-product-reviews.php:139
msgid "Rate&hellip;"
msgstr ""

#: woocommerce/single-product-reviews.php:140
msgid "Perfect"
msgstr ""

#: woocommerce/single-product-reviews.php:141
msgid "Good"
msgstr ""

#: woocommerce/single-product-reviews.php:142
msgid "Average"
msgstr ""

#: woocommerce/single-product-reviews.php:143
msgid "Not that bad"
msgstr ""

#: woocommerce/single-product-reviews.php:144
msgid "Very Poor"
msgstr ""

#: woocommerce/single-product-reviews.php:149
msgid "Your Review"
msgstr ""

#: page-templates/parts/sharebox.php:21
msgid "Share on facebook"
msgstr ""

#: page-templates/parts/sharebox.php:28
msgid "Share on Twitter"
msgstr ""

#: page-templates/parts/sharebox.php:35
msgid "Share on LinkedIn"
msgstr ""

#: page-templates/parts/sharebox.php:42
msgid "Share on Tumblr"
msgstr ""

#: page-templates/parts/sharebox.php:50
msgid "Share on Google plus"
msgstr ""

#: page-templates/parts/sharebox.php:58
msgid "Share on Pinterest"
msgstr ""

#: page-templates/parts/sharebox.php:65
msgid "Email to a Friend"
msgstr ""

#: page-templates/parts/topbar-02.php:22 page-templates/parts/topbar.php:37
msgid "Welcome "
msgstr ""

#: page-templates/parts/topbar.php:9 widgets/contact-info/default.php:78
#: widgets/contact-info/default.php:81 widgets/contact-info/default.php:111
msgid "Call support free:"
msgstr ""

#: page-templates/parts/vertical.php:2
msgid "All Category"
msgstr ""

#: widgets/contact-info/default.php:7 inc/vendors/kingcomposer/functions.php:27
#: inc/vendors/kingcomposer/functions.php:81
#: inc/vendors/kingcomposer/functions.php:119
#: inc/vendors/kingcomposer/functions.php:514
msgid "Title"
msgstr ""

#: widgets/contact-info/default.php:8
msgid "Description"
msgstr ""

#: widgets/contact-info/default.php:9
msgid "Company"
msgstr ""

#: widgets/contact-info/default.php:10
msgid "Country"
msgstr ""

#: widgets/contact-info/default.php:11
msgid "Locality"
msgstr ""

#: widgets/contact-info/default.php:12
msgid "Region"
msgstr ""

#: widgets/contact-info/default.php:13
msgid "Street"
msgstr ""

#: widgets/contact-info/default.php:14
msgid "Phone"
msgstr ""

#: widgets/contact-info/default.php:15
msgid "Mobile"
msgstr ""

#: widgets/contact-info/default.php:16
msgid "Fax"
msgstr ""

#: widgets/contact-info/default.php:17
msgid "Skype"
msgstr ""

#: widgets/contact-info/default.php:18
msgid "Email Address"
msgstr ""

#: widgets/contact-info/default.php:20
msgid "Working Hours"
msgstr ""

#: widgets/contact-info/default.php:21
msgid "Working Days"
msgstr ""

#: widgets/contact-info/default.php:22
msgid "Website URL"
msgstr ""

#: widgets/contact-info/default.php:23
msgid "Website"
msgstr ""

#: widgets/contact-info/default.php:67
msgid "head Office"
msgstr ""

#: widgets/contact-info/default.php:95 widgets/contact-info/default.php:97
msgid "Email us:"
msgstr ""

#: woocommerce/cart/cart.php:35 woocommerce/cart/cart.php:78
msgid "Product"
msgstr ""

#: woocommerce/cart/cart.php:36 woocommerce/cart/cart.php:96
msgid "Price"
msgstr ""

#: woocommerce/cart/cart.php:37 woocommerce/cart/cart.php:102
msgid "Quantity"
msgstr ""

#: woocommerce/cart/cart.php:38 woocommerce/cart/cart.php:119
msgid "Total"
msgstr ""

#: woocommerce/cart/cart.php:59 woocommerce/cart/mini-cart.php:63
msgid "Remove this item"
msgstr ""

#: woocommerce/cart/cart.php:91
msgid "Available on backorder"
msgstr ""

#: woocommerce/cart/cart.php:139
msgid "Coupon:"
msgstr ""

#: woocommerce/cart/cart.php:139
msgid "Coupon code"
msgstr ""

#: woocommerce/cart/cart.php:139
msgid "Apply Coupon"
msgstr ""

#: woocommerce/cart/cart.php:145
msgid "Update Cart"
msgstr ""

#: woocommerce/cart/cross-sells.php:41
msgid "You may be interested in&hellip;"
msgstr ""

#: woocommerce/cart/mini-cart-button-market-v1.php:5
#: woocommerce/cart/mini-cart-button.php:4
msgid "View your shopping cart"
msgstr ""

#: woocommerce/cart/mini-cart-button-market-v1.php:8
msgid "Cart: "
msgstr ""

#: woocommerce/cart/mini-cart-button.php:6
#: inc/vendors/woocommerce/functions.php:159
#, php-format
msgid " <span class=\"mini-cart-items\">%d</span> "
msgid_plural " <span class=\"mini-cart-items\">%d</span> "
msgstr[0] ""
msgstr[1] ""

#: woocommerce/cart/mini-cart.php:82
msgid "No products in the cart."
msgstr ""

#: woocommerce/cart/mini-cart.php:90
msgid "Subtotal"
msgstr ""

#: woocommerce/cart/mini-cart.php:95
msgid "View Cart"
msgstr ""

#: woocommerce/cart/mini-cart.php:96
msgid "Checkout"
msgstr ""

#: woocommerce/checkout/form-checkout.php:20
msgid "You must be logged in to checkout."
msgstr ""

#: woocommerce/checkout/form-checkout.php:51
msgid "Your order"
msgstr ""

#: woocommerce/loop/add-to-cart.php:20
msgid "Add to cart"
msgstr ""

#: woocommerce/single-product/related.php:48
msgid "Related Products"
msgstr ""

#: woocommerce/single-product/review.php:30
msgid "Your comment is awaiting approval"
msgstr ""

#: woocommerce/single-product/review.php:36
msgid "verified owner"
msgstr ""

#: woocommerce/single-product/review.php:43
#, php-format
msgid "Rated %d out of 5"
msgstr ""

#: woocommerce/single-product/review.php:44
msgid "out of 5"
msgstr ""

#: woocommerce/single-product/up-sells.php:43
msgid "You may also like&hellip;"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:19
msgid "FAQ"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:23
#: inc/vendors/kingcomposer/functions.php:115
msgid "Elements"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:34
#: inc/vendors/kingcomposer/functions.php:74
msgid "Options"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:36
msgid ""
"Repeat this fields with each item created, Each item corresponding element."
msgstr ""

#: inc/vendors/kingcomposer/functions.php:37
msgid "Add new item"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:42
msgid "Question"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:49
msgid "Answer"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:60
msgid "Enter class name for wrapper"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:66
msgid "History About"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:76
#: inc/vendors/kingcomposer/functions.php:180
msgid ""
"Repeat this fields with each item created, Each item corresponding "
"processbar element."
msgstr ""

#: inc/vendors/kingcomposer/functions.php:77
msgid "Add new question"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:83
msgid "Enter text used as title of bar."
msgstr ""

#: inc/vendors/kingcomposer/functions.php:90
msgid "Enter text used as content of bar."
msgstr ""

#: inc/vendors/kingcomposer/functions.php:111
msgid "Menu Vertical"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:126
msgid "Menu select"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:129
msgid "Select menu."
msgstr ""

#: inc/vendors/kingcomposer/functions.php:134
msgid "Position"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:140
msgid "Postion Menu Vertical."
msgstr ""

#: inc/vendors/kingcomposer/functions.php:144
#: inc/vendors/kingcomposer/functions.php:220
#: inc/vendors/kingcomposer/functions.php:334
#: inc/vendors/kingcomposer/functions.php:548
msgid "Extra class name"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:146
#: inc/vendors/kingcomposer/functions.php:222
#: inc/vendors/kingcomposer/functions.php:336
#: inc/vendors/kingcomposer/functions.php:550
msgid ""
"If you wish to style particular content element differently, then use this "
"field to add a class name and then refer to it in your css file."
msgstr ""

#: inc/vendors/kingcomposer/functions.php:156
msgid "Ourservice"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:161
msgid "Ourservice."
msgstr ""

#: inc/vendors/kingcomposer/functions.php:166
#: inc/vendors/kingcomposer/functions.php:287
#: inc/vendors/kingcomposer/functions.php:310
#: inc/vendors/kingcomposer/functions.php:343
msgid "Style"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:168
msgid "Select the style of progress bars."
msgstr ""

#: inc/vendors/kingcomposer/functions.php:170
msgid " Style 1 (Ourservice Vertical)"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:171
msgid " Style 2 (Ourservice Horizontal)"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:178
msgid " Options"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:181
msgid " Add new items Ourservice"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:204
msgid "Label"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:206
msgid "Enter text used as title of the bar."
msgstr ""

#: inc/vendors/kingcomposer/functions.php:211
msgid "Information"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:231
msgid "Select Categories"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:236
msgid ""
"Select category which you chosen for posts ( hold ctrl or shift to select "
"multiple )"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:279
#: inc/vendors/kingcomposer/functions.php:314
#: inc/vendors/kingcomposer/functions.php:326
#: inc/vendors/woocommerce/functions.php:277
msgid "Grid"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:280
#: inc/vendors/kingcomposer/functions.php:313
#: inc/vendors/woocommerce/functions.php:278
msgid "List"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:281
msgid "list 2"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:300
msgid "Style 1"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:301
msgid "Style 2"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:322
msgid "Layout"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:325
msgid "Carousel"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:352
msgid "Row Carousel Products"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:371
msgid "Display Product Deals in grid"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:378
#: inc/vendors/kingcomposer/functions.php:453
msgid "Select Category"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:380
#: inc/vendors/kingcomposer/functions.php:455
msgid "Select Category to display"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:402
#: inc/vendors/kingcomposer/functions.php:534
msgid "Grid Column"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:421
msgid "Display Bestseller, Latest, Most Review ... in grid"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:428
msgid "Product main position"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:432
msgid "Top"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:433
msgid "Left"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:434
msgid "Right"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:444
msgid "Recent Products"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:445
msgid "Sale Products"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:446
msgid "Featured Products"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:447
msgid "Best Selling Products"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:448
msgid "Products"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:493
msgid "1 column product"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:494
msgid "2 column product"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:506
msgid "Display Shop Category"
msgstr ""

#: inc/vendors/kingcomposer/functions.php:521
msgid "Number categories"
msgstr ""

#: inc/vendors/woocommerce/functions.php:168
#: inc/vendors/woocommerce/functions.php:172
msgid "Add To Wishlist"
msgstr ""

#: inc/vendors/woocommerce/functions.php:508
msgid "In Stock"
msgstr ""

#. Name of the theme
msgid "Evolution"
msgstr ""

#. Description of the theme
msgid ""
"The evolution theme for WordPress by Wpopal.Com and Based On <a href=\"http:"
"//bit.ly/OpalWP\">Opal Framework</a> is a fully responsive theme that looks "
"great on any device. That's the best for any kind of online shoping sites."
msgstr ""

#. URI of the theme
msgid "http://www.wpopal.com?theme=evolution"
msgstr ""

#. Author of the theme
msgid "Wpopal Team"
msgstr ""

#. Author URI of the theme
msgid "http://www.wpopal.com"
msgstr ""
