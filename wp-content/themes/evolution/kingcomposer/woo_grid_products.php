<?php 
	$atts  = array_merge( array(
		'per_page'  => 8,
		'columns'	=> 4,
		'type'		=> 'recent_products',
		'category'	=> '',
		'woocategory' => '',
	), $atts); 
	extract( $atts );	
	if( empty($type) ){
		return ;
	}


	switch ($columns) {
        case '5':
            $class_column='col-sm-6 col-md-2-4';
            break;
        case '4':
            $class_column='col-sm-6 col-md-3';
            break;
        case '3':
            $class_column='col-sm-6 col-md-4';
            break;
        case '2':
            $class_column='col-sm-6 col-md-6';
            break;
        default:
            $class_column='col-md-12';
            break;
    }

	if ( isset($woocategory) && !empty($woocategory) ){
		$categories = array_keys( wpopal_themer_autocomplete_options_helper($woocategory) );
	}else {
		$categories = '';
	}


	$loop = wpopal_themer_woocommerce_query($type, $per_page, $categories);
    
	if ($style=='grid') {
        $style = 'inner';
    }elseif ($style=='List-v2') {
        $style = 'list-v2';
    }else {
        $style = 'list';
    }

	$_count = 0;

?>

 <div class="widget_products">
    <div class="products-<?php echo esc_attr($style);?> <?php echo esc_attr($product_style);?>">
        <?php $_count =0; while ( $loop->have_posts() ) : $loop->the_post(); ?>


                <div class="product-wrapper product <?php echo esc_attr(  $class_column );?><?php echo ($_count%$columns==0 && $columns !=1)? ' first' : ''; ?><?php echo ($_count < $columns)? ' top-product' : ''; ?>">
                   <?php wc_get_template_part( 'content', 'product-'.$style ); ?>
                </div>

                <?php
                $_count++; ?>
    	<?php endwhile; ?>
    	<?php wp_reset_postdata(); ?>
        <div class="clearfix"></div>
    </div>
</div> 