<?php
/**
 * Single Product Up-Sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version      3.3.1
 */
if( !(evolution_fnc_theme_options('wc_show_upsells', false)) ){
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	global $product, $woocommerce_loop;

	$upsells = $product->get_upsell_ids();
	$posts_per_page = evolution_fnc_theme_options('woo-number-product-single',6);
	if ( sizeof( $upsells ) == 0 ) return;

	$meta_query = WC()->query->get_meta_query();

	$args = array(
		'post_type'           => 'product',
		'ignore_sticky_posts' => 1,
		'no_found_rows'       => 1,
		'posts_per_page'      => $posts_per_page,
		'orderby'             => $orderby,
		'post__in'            => $upsells,
		'post__not_in'        => array( $product->get_id() ),
		'meta_query'          => $meta_query
	);
	$_count =1;
	$products = new WP_Query( $args );
	$columns_count = evolution_fnc_theme_options('product-number-columns',3);
	$class_column = 'col-sm-' . floor( 12/$columns_count );
	$woocommerce_loop['columns'] = $columns;
	$_id = rand();
	
	if ( $products->have_posts() ) : ?>

			<div class="widget products-collection owl-carousel-play woocommerce" id="postcarousel-<?php echo esc_attr($_id); ?>" data-ride="carousel">
			 
					<div class="mask"></div>
					<h3 class="widget-title">
				        <span><?php esc_html_e( 'You may also like&hellip;', 'evolution' ); ?></span>
					</h3>
					<div class="woocommerce">
						<div class="widget-content <?php echo isset($style) ? esc_attr( $style ): ''; ?>">
						    <?php   if( $products->post_count > $columns_count ) { ?>
							<div class="carousel-controls carousel-controls-v2 carousel-hidden">
								<a href="#postcarousel-<?php echo esc_attr($_id); ?>" data-slide="prev" class="left carousel-control carousel-md">
									<span class="fa fa-angle-left"></span>
								</a>
								<a href="#postcarousel-<?php echo esc_attr($_id); ?>" data-slide="next" class="right carousel-control carousel-md">
									<span class="fa fa-angle-right"></span>
								</a>
							</div>
							<?php } ?>
	
						    <div class="owl-carousel " data-slide="<?php echo esc_attr($columns_count); ?>"  data-singleItem="true" data-navigation="false" data-pagination="false">
								<?php while ( $products->have_posts() ) : $products->the_post();  ?>
										<div class="product-carousel-item">	<?php wc_get_template_part( 'content', 'product-inner' ); ?></div>
								<?php endwhile; ?>
							</div>

						</div>
					</div>
				 
			</div>

		<?php endif;

	wp_reset_postdata();
}