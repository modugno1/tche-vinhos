<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WpOpal
 * @subpackage evolution
 * @since evolution 1.0
 */
 

?>
		</section><!-- #main -->
		<?php do_action( 'evolution_template_main_after' ); ?>
		<?php do_action( 'evolution_template_footer_before' ); ?>
		<footer id="opal-footer" class="opal-footer" role="contentinfo">
			<?php echo evolution_display_footer_content(); ?>
			
			<div class="container">
				<div class="row">
				<?php if(is_active_sidebar('mass-footer-body')): ?>
					<?php get_sidebar( 'mass-footer-body' );  ?>
				<?php endif ; ?>	
				</div>
			</div>
			<section class="opal-copyright clearfix">
				<div class="container">
					<div class="row">
						<a href="#" class="scrollup"><span class="fa fa-angle-up"></span></a>
						<?php do_action( 'evolution_fnc_credits' ); ?>
						<div class="copyright-link nav-menu hidden-xs"><?php dynamic_sidebar( 'copyright-link' ); ?></div>
						<div class="text-copyright text-center col-sm-12 col-md-12">
							<?php 
								evolution_display_footer_copyright();
							?>
						</div>
						
					</div>
					
				</div>	
			</section>
		</footer><!-- #colophon -->
		

		<?php do_action( 'evolution_template_footer_after' ); ?>
		<?php get_sidebar( 'offcanvas' );  ?>
	</div>
</div>
	<!-- #page -->

<?php wp_footer(); ?>
</body>
</html>