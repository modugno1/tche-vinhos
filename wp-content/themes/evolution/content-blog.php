<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WpOpal
 * @subpackage evolution
 * @since evolution 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	
	<div class="content-blog">
		<div class="image-thumnail">
			<?php evolution_fnc_post_thumbnail(); ?>
		</div>

		<?php 
		the_title( '<h3 class="title-post"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		?>
		<div class="bottom-blog">
			<div class="entry-date pull-left">
		        <span><?php the_time( 'd' ); ?></span>&nbsp;<?php the_time( 'M' ); ?>
		   </div>
		   <div class="author pull-right"><i class="fa fa-pencil" aria-hidden="true"></i><?php esc_html_e('Post by:', 'evolution');?></i><?php the_author_posts_link(); ?></div>
		</div>
		
	</div>
	

	
 
	<?php //the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
</article><!-- #post-## -->
