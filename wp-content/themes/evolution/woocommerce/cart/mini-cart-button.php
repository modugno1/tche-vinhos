<?php   global $woocommerce; ?>
<div class="opal-topcart">
 <div id="cart" class="dropdown version-1 box-top btn">
        <a class="dropdown-toggle mini-cart box-wrap label-title" data-toggle="dropdown" aria-expanded="true" role="button" aria-haspopup="true" data-delay="0" href="#" title="<?php esc_html_e('View your shopping cart', 'evolution'); ?>">
            <span class="before-cart">
                <?php echo sprintf(_n(' <span class="mini-cart-items">%d</span> ', ' <span class="mini-cart-items">%d</span> ', $woocommerce->cart->cart_contents_count, 'evolution'), $woocommerce->cart->cart_contents_count);?> <?php echo trim( $woocommerce->cart->get_cart_total() ); ?>
            </span>
            <p class="label-title__text label-title__cart">Carrinho</p>
        </a>            
        <div class="dropdown-menu"><div class="widget_shopping_cart_content">
            <?php woocommerce_mini_cart(); ?>
        </div></div>
    </div>
</div> 
