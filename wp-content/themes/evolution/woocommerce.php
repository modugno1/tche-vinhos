<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WpOpal
 * @subpackage evolution
 * @since evolution 1.0
 */

$evolution_page_layouts = apply_filters( 'evolution_fnc_get_woocommerce_sidebar_configs', null );

get_header( apply_filters( 'evolution_fnc_get_header_layout', null ) );

if( is_singular('product') ) {
 $bgimage = evolution_fnc_theme_options( 'woocommerce-single-breadcrumb' ); 
 $style = array();
 if( $bgimage  ){ 
  $style[] = 'background-image:url(\''. $bgimage .'\')';
 }
 $estyle = !empty($style)? 'style="'.implode(";", $style).'"':"";
}
if ( !isset($estyle) || !$estyle ) {
 $bgimage = evolution_fnc_theme_options( 'breadcrumb-bg' ); 
 $style = array();
 if( $bgimage  ){ 
  $style[] = 'background-image:url(\''. $bgimage .'\')';
 }
 $estyle = !empty($style)? 'style="'.implode(";", $style).'"':"";
}
?>

<div id="opal-breadscrumb" <?php echo isset($estyle) ? $estyle : ''; ?>>
  <?php do_action( 'evolution_woo_template_main_before' ); ?>
</div>
<section id="main-container" class="<?php echo apply_filters('evolution_template_woocommerce_main_container_class','container');?>">
	
	<div class="row">
		
		<?php if( isset($evolution_page_layouts['sidebars']) && !empty($evolution_page_layouts['sidebars']) ) : ?>
			<?php get_sidebar(); ?>
		<?php endif; ?>

		<div id="main-content" class="main-content col-xs-12 <?php echo esc_attr($evolution_page_layouts['main']['class']); ?>">
			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">
				<?php  
				 if ( is_singular( 'product' ) ) {

		            while ( have_posts() ) : the_post();

		                wc_get_template_part( 'content', 'single-product' );

		            endwhile;

		        } else { ?>
		        		<div class="woocommerce-archive-content">
			          	<?php if(evolution_fnc_theme_options('archive-show-title', true)){ ?>

			            	<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

			            <?php } ?>

			            <?php if(evolution_fnc_theme_options('archive-show-banner', true)){ ?>

			            	<?php do_action( 'woocommerce_archive_description' ); ?>

			            <?php } ?>

		            

		            	<?php if ( have_posts() ) : ?>

		                	<?php do_action('woocommerce_before_shop_loop'); ?>
		                	<div class="childrens childrens-<?php echo esc_attr(get_option('woocommerce_shop_page_display')); ?>">

		                    	<?php woocommerce_product_subcategories(); ?>
		                	</div>
		               	<?php woocommerce_product_loop_start(); ?>
		                   
		                  <?php while ( have_posts() ) : the_post(); ?>

		                     <?php wc_get_template_part( 'content', 'product' ); ?>

		                  <?php endwhile; // end of the loop. ?>

		                	<!-- <?php woocommerce_product_loop_end(); ?> -->

		                	<?php do_action('woocommerce_after_shop_loop'); ?>

		            	<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

		                	<?php wc_get_template( 'loop/no-products-found.php' ); ?>
		                
		            	<?php endif;?>
		            
		            </div>
            <?php
		         }
				?>

				</div><!-- #content -->
			</div><!-- #primary -->


			<?php    get_sidebar( 'content' ); ?>
		</div><!-- #main-content -->

		

	</div>
		
</section>
<?php if ( !is_singular( 'product' ) ): ?>
			<div class="woocommerce-footer">
				<?php if ( is_active_sidebar( 'woocommerce-footer-body' ) ) : ?>
					<div class="widget-area">
						<?php dynamic_sidebar( 'woocommerce-footer-body' ); ?>
					</div><!-- .widget-area -->
				<?php endif; ?>
			</div>
		<?php endif; ?>
<?php

get_footer();
