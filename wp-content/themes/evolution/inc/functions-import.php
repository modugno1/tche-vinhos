<?php

function evolution_fnc_import_remote_demos() { 
	return array(
		'evolution' => array( 
			'name' 		=> 'evolution', 
		 	'source'	=> esc_url( 'http://wpsampledemo.com/evolution/evolution.zip'),
		 	'preview'   => esc_url( 'http://wpsampledemo.com/evolution/screenshot.png')
		),
	);
}

add_filter( 'wpopal_themer_import_remote_demos', 'evolution_fnc_import_remote_demos' );



function evolution_fnc_import_theme() {
	return 'evolution';
}
add_filter( 'wpopal_themer_import_theme', 'evolution_fnc_import_theme' );

function evolution_fnc_import_demos() {
	$folderes = glob( get_template_directory() .'/inc/import/*', GLOB_ONLYDIR ); 

	$output = array(); 

	foreach( $folderes as $folder ){
		$output[basename( $folder )] = basename( $folder );
	}
 	
 	return $output;
}
add_filter( 'wpopal_themer_import_demos', 'evolution_fnc_import_demos' );

function evolution_fnc_import_types() {
	return array(
			'all'          => esc_html__( 'All' , 'evolution' ),
			'content'      =>  esc_html__( 'Content', 'evolution' ),
			'widgets'      => esc_html__( 'Widgets', 'evolution' ) ,
			'page_options' => esc_html__( 'Theme + Page Options', 'evolution' ) ,
			'menus'        => esc_html__( 'Menus', 'evolution' ),
			'rev_slider'   => esc_html__( 'Revolution Slider', 'evolution' ),
			'vc_templates' => esc_html__( 'VC Templates', 'evolution' )
		);
}
add_filter( 'wpopal_themer_import_types', 'evolution_fnc_import_types' );

/**
 * Matching and resizing images with url.
 *
 *  $ouput = array(
 *        'allowed' => 1, // allow resize images via using GD Lib php to generate image
 *        'height'  => 900,
 *        'width'   => 800,
 *        'file_name' => 'blog_demo.jpg'
 *   ); 
 */
function evolution_import_attachment_image_size( $url ){  

   $name = basename( $url );   
 
   $ouput = array(
         'allowed' => 0
   );     
   
   if( preg_match("#product#", $name) ) {
      $ouput = array(
         'allowed' => 1,
         'height'  => 475,
         'width'   => 600,
         'file_name' => 'product_demo.jpg'
      ); 
   }
   elseif( preg_match("#blog#", $name) ){
      $ouput = array(
         'allowed' => 1,
         'height'  => 500,
         'width'   => 900,
         'file_name' => 'blog_demo.jpg'
      ); 
   }
   return $ouput;
}

add_filter( 'pbrthemer_import_attachment_image_size', 'evolution_import_attachment_image_size' , 1 , 999 );