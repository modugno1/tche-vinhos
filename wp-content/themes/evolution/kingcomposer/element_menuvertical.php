<?php
	extract( $atts );

	$nav_menu = ( $menu !='' ) ? wp_get_nav_menu_object( $menu ) : false;
	$menuid  = is_object($nav_menu)&& $nav_menu->slug ? $nav_menu->slug : 'topmenu';
	
	if(!$nav_menu) return false;
	$postion_class = ($position=='left')?'menu-left':'menu-right';
	$args = array(  'menu' => $menuid,
	                'container_class' => 'collapse navbar-collapse navbar-ex1-collapse vertical-menu hidden-xs hidden-sm '.$postion_class,
	                'menu_class' => 'nav navbar-nav navbar-vertical-mega megamenu',
	                'fallback_cb' => ''
                );

    if( class_exists("Wpopal_Themer_Megamenu_Vertical") ){

        $args['walker'] = new Wpopal_Themer_Megamenu_Vertical($nav_menu->term_id);
    }

?>


<?php if( !empty( $title) ): ?>
	<h3 class="widget-title visual-title"><span><?php echo trim( $title ); ?></span></h3>
<?php endif; ?>

<?php wp_nav_menu($args); ?>
