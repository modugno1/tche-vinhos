<?php
/**
 * The Header for our theme: Main Darker Background. Logo left + Main menu and Right sidebar. Below Category Search + Mini Shopping basket.
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WpOpal
 * @subpackage evolution
 * @since evolution 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site"><div class="opal-page-inner row-offcanvas row-offcanvas-left">
	<?php if ( get_header_image() ) : ?>
	<div id="site-header" class="hidden-xs hidden-sm">
		<a href="<?php echo esc_url( get_option('header_image_link','#') ); ?>" rel="home">
			<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
		</a>
	</div>
	<?php endif; ?>
	<?php get_template_part( 'page-templates/parts/topbar', 'mobile' ); ?>
	
	<?php get_template_part( 'page-templates/parts/topbar-02'); ?>
	
	<header id="opal-masthead" class="site-header box-shadow-none" role="banner">
	
		
	<section class="header-main v2">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-lg-3 col-xs-12">
		 			<?php if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ): ?>
						<div id="opal-logo" class="logo ">
							<?php the_custom_logo(); ?>
						</div>
					<?php else: ?>
					    <div id="opal-logo" class="logo logo-theme">
					        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					             <img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/logo.png'; ?>" alt="<?php bloginfo( 'name' ); ?>" />
					        </a>
					    </div>
					<?php endif; ?>
				</div>
				
				<?php if ( is_active_sidebar( 'contact-header' ) ) : ?>
					<div class="col-lg-9 col-md-9 col-xs-12 hidden-xs hidden-sm">
                 <?php dynamic_sidebar( 'contact-header' ); ?>
               </div>  
            <?php endif; ?>
			
					
				

				
			</div>				
		</div>
	</section>

	<section class="header-bottom v2 <?php echo evolution_fnc_theme_options('keepheader') ? 'has-sticky' : 'no-sticky'; ?> hidden-xs hidden-sm">
		<div class="container">
			 <div class="display-flex border-top position-relative">

				<div class="opal-mainmenu ">
					<div class="inner navbar-mega-simple"><?php get_template_part( 'page-templates/parts/nav' ); ?></div>
				</div>
					
				<div class="header-right">
					<div id="search-container" class="search-box-wrapper">
						<?php get_template_part( 'page-templates/parts/search-overlay-v2' ); ?>
					</div>
				      
					<?php do_action( "evolution_template_header_right"); ?>

				</div>
			</div>
		</div>
		
	</section>	


	<!-- Main Menu -->
		
	

	</header><!-- #masthead -->	

	<?php do_action( 'evolution_template_header_after' ); ?>
	
	<section id="main" class="site-main">