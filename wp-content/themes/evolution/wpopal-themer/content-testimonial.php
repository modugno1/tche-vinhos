<?php
    $link = get_post_meta( get_the_ID(), 'testimonials_link', true );
    $job = get_post_meta( get_the_ID(), 'testimonials_job', true );
    $excerpt = explode(' ', strip_tags(get_the_content( )), 100);
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);

?>
<div class="testimonials">
    <div class="testimonials-body">
                                   
        <ul class="testimonials-avatar list-unstyled">
            <li class="active">
                
            </li>                       
        </ul>
        <p class="testimonials-description"><?php echo trim( $excerpt ); ?></p>                        
        
        <div class="testimonials-bottom">
            <div class="testimonials-avatar list-unstyled">
                <div class="avatar"><?php the_post_thumbnail('evolution-avatar', '', 'class="radius-x"');?></div>
            </div>
            <div class="testimonials-info">
                <h5 class="testimonials-name">
                    <?php the_title(); ?>
                </h5>
                <p class="text-muted testimonials-position">
                    <a href="<?php echo empty($link) ? '#' : esc_url( $link ); ?>">
                        <?php echo empty($job) ? '' : trim( $job ); ?>
                    </a>
                </p>
            </div>
             
        </div>  
         
    </div>                      
</div>