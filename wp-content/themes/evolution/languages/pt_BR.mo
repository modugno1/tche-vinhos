��    @        Y         �     �     �     �     �     �  (   �  *   �  =     *   P  0   {  *   �     �     �     �     �               '     6     E     J     P     W     e     r          �     �     �     �  	   �     �     �     �  W   �     ,  
   9  	   D     N  
   T     _     p     w  8   �     �  	   �     �     �     �  F   �     F	     X	     k	     y	  \   �	     �	     �	  &   
     /
     H
  $   Q
  )   v
     �
  Y  �
     �               )     5  #   G  #   k  ;   �  '   �  (   �  .        K     T     \     r     �     �     �     �     �     �     �     �     �       	   !  	   +  	   5  	   ?  	   I     S     \  
   s     ~  _   �     �     �                    )  	   B     L  <   U  	   �     �     �     �     �  .   �               :     J  a   ^     �     �     �             $   %     J     j            
             -       :         6   ;       ?                  +             >         4                     2   0                 %       @       8   <          1             	                     5       /      $   3   .       )   !   (   "                         9       =       ,          '      #   &   7   *     - Edit  - Reply  Topbar Background 404 Page All posts by %s Appears in the Home section of the site. Appears in the Topbar section of the site. Appears in the end of woocommerce footer section of the site. Appears in the footer section of the site. Appears in the header right section of the site. Appears on posts and pages in the sidebar. Archives Basic Blog Left Sidebar Blog Right Sidebar Colors Comments are closed. Contact Header Custom Service Edit Email Email: Enable Banner Enable Title Enable title Footer 1 Footer 2 Footer 3 Footer 4 Footer 5 Galleries General Settings Header Inner It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Left Sidebar Main Color Main menu Name: Next Image Page Not Found ! Pages: Phone Number Please drag and drop widget on this position Static Left Post by: Posted by Previous Image Primary Color Product zoom mode Ready to publish your first post? <a href="%1$s">Get started here</a>. Remove Box Shadow Return to homepage Right Sidebar Search Results for: %s Sorry, but nothing matched your search terms. Please try again with some different keywords. Topbar color Vertical Menu We are sorry, but something went wrong Website General Settings Website: Your comment is awaiting moderation. Your email address will not be published. by Project-Id-Version: Opal Theme
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=(n > 1);
POT-Creation-Date: 2017-10-18 04:16+0000
PO-Revision-Date: 2018-03-24 09:04-0300
X-Generator: Poedit 2.0.6
Language-Team: 
Last-Translator: 
Language: pt_BR
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 - Editar - Responder Fundo da barra do topo Página 404 Todos posts de %s Aparecer a home na seção do site. Aparecer na seção topbar do site. Aparece no final da seção de rodapé woocommerce do site. Aparecer na seção do rodapé do site. Aparecer o Cabeçalho na direita do site Aparecer em posts e páginas na barra lateral. Arquivos Básico Sidebar esquerda Blog Sidebar direita Blog Cores Comentários estão fechados. Contato do Cabeçalho Personalizar Serviço Editar E-mail E-mail Habilitar banner Habilitar título Habilitar título Rodapé 1 Rodapé 2 Rodapé 3 Rodapé 4 Rodapé 5 Galerias Configurações gerais Cabeçalho Interno Parece que não podemos encontrar o que você está procurando. Talvez a pesquisa possa ajudar. Sidebar esquerdo Cor Principal Menu Principal Nome Próxima Imagem Página Não Encontrada! Páginas: Telefone Arraste e solte o widget nesta posição. Esquerda estática Posts de: Postado por: Imagem Anterior Primeira cor Produto modo Zoom Pronto para seu primeiro post? Comece por aqui Remover sombra da caixa Retornar a página inicial Sidebar direito Resultados Para: %s Desculpe, mas não encontramos nenhum resultado. Por favor tente com algumas palavras diferentes. Cor da barra do topo Menu vertical Desculpe, mas algo deu errado Configurações gerais do site Site Seu comentário aguarda moderação. Seu e-mail não será publicado Por 