<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WpOpal
 * @subpackage evolution
 * @since evolution 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php evolution_fnc_post_thumbnail(); ?>

	<div class="bottom blogv1">

			<div class="title-post">
			<?php 
				the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			?>
			</div>
		
			<div class="entry-meta">
			
				<div class="entry-date">
				
			         <span><?php the_time( 'd' ); ?></span>&nbsp;<?php the_time( 'M' ); ?>
			   </div>

			   <div class="author"> <span><?php esc_html_e('Posted by', 'evolution');?></span> <?php the_author_posts_link(); ?></div>

			</div>
		
	</div>
 
</article><!-- #post-## -->
