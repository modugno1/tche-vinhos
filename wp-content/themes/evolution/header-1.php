<?php
/**
 * The Header for our theme: Main Darker Background. Logo left + Main menu and Right sidebar. Below Category Search + Mini Shopping basket.
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WpOpal
 * @subpackage evolution
 * @since evolution 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site"><div class="opal-page-inner row-offcanvas row-offcanvas-left">
	<?php if ( get_header_image() ) : ?>
	<div id="site-header" class="hidden-xs hidden-sm">
		<a href="<?php echo esc_url( get_option('header_image_link','#') ); ?>" rel="home">
			<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
		</a>
	</div>
	<?php endif; ?>
	<?php get_template_part( 'page-templates/parts/topbar', 'mobile' ); ?>
	
	<section id="opal-topbar" class="opal-topbar hidden-xs hidden-sm">
    		<?php get_template_part( 'page-templates/parts/topbar'); ?>
	</section>
	
	<header id="opal-masthead" class="site-header" role="banner">
	
		
	<section class="header-main ">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-lg-3 col-xs-12">
		 			<?php get_template_part( 'page-templates/parts/logo' ); ?>
				</div>
				<div class="col-lg-9 col-md-9 col-xs-12 hidden-xs hidden-sm display-flex">

					<div id="search-container" class="search-box-wrapper pull-left">
						<?php get_template_part( 'page-templates/parts/search-overlay' ); ?>
					</div>
					
					<div class="header-right">

						<?php if( class_exists( 'YITH_WCWL' ) ): ?>
				      <a class="opal-btn-wishlist label-title" href="<?php echo get_permalink( get_option('yith_wcwl_wishlist_page_id') ); ?>">
					  	<i class="fa fa-heart-o" aria-hidden="true"></i>
						<p class="label-title__text label-title__wish">Favoritar</p>
					</a>
				      <?php endif; ?>
				      
						<?php do_action( "evolution_template_header_right"); ?>

					</div>
				</div>
						
			</div>				
		</div>
	</section>

	<!-- Main Menu -->
	<section class="main-menu <?php echo evolution_fnc_theme_options('keepheader') ? 'has-sticky' : 'no-sticky'; ?>">
		<div class="container">
			<div id="opal-mainmenu" class="opal-mainmenu">
				
				<div class="inner navbar-mega-simple"><?php get_template_part( 'page-templates/parts/nav' ); ?></div>
				
			</div>
		</div>
		

		
	</section>	
	

	</header><!-- #masthead -->	

	<?php do_action( 'evolution_template_header_after' ); ?>
	
	<section id="main" class="site-main">