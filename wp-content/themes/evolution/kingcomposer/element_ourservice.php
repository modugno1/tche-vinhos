<?php

$style = $class = $align = $css = $title_style = '';
$atts  = array_merge( array(
    'style'       => '',
    'label'       => '',
		'options'     => '',
		'info'        => '',
		'el_class' => '',
	), $atts); 
extract( $atts );
$_count = 0;
 
?>

<div class="opal-ourservice <?php echo esc_attr($el_class) ?>">
 <ul class="ourservice v<?php echo esc_attr($style) ?>">
  <?php
  
      if (isset($options)) {
        foreach ($options as $option) {
          $_count++;

          $label = !empty($option->label) ? $option->label : '';
          $info = !empty($option->info) ? $option->info : '';
           
          ?>
          <li class="ourservice-box">
            <span class="count"><?php echo trim($_count); ?></span>
            <h3 class="title"><?php echo trim($label); ?></h3>
            <p class="info"><?php echo trim($info); ?></p>
          </li>
            

          <?php
           
        }
      }
  ?>

  
   
  </ul>
</div>







