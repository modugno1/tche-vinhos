<?php

/**
 * Load woocommerce styles follow theme skin actived
 *
 * @static
 * @access public
 * @since Wpopal_Themer 1.0
 */
function evolution_fnc_woocommerce_get_image_thumbnails($product,$idrand) {
	$id = rand();
	$images = $product->get_gallery_image_ids();

	$attachment_ids =  array_merge_recursive( array( get_post_thumbnail_id() ) , $images ) ;
	 
	if ( $attachment_ids ) {
	    $loop       = 0;
	    $columns    = apply_filters( 'woocommerce_product_thumbnails_columns', 4);
	    ?>
	    <div id="image-additional-carousel">
	         <div class="image-additional olw-carousel  owl-carousel-play" id="image-additional"   data-ride="owlcarousel">     
	             <div class="owl-carousel" data-slide="1" data-pagination="false" data-navigation="true">

	        <?php  $collection = array_chunk( $attachment_ids, $columns ); ?> 

	        <?php
	        foreach( $collection as  $ids ){

	            echo '<div class="hitem">';
	            foreach ( $ids as $attachment_id ) {

	                $classes = array( 'image-'.$product->get_id().$idrand );

	                if ( $loop == 0 || $loop % $columns == 0 )
	                    $classes[] = 'first';

	                if ( ( $loop + 1 ) % $columns == 0 )
	                    $classes[] = 'last';

	                $image_link = wp_get_attachment_url( $attachment_id );

	                if ( ! $image_link )
	                    continue;

	                $image_title    = esc_attr( get_the_title( $attachment_id ) );
	                $image_caption  = esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

	                $image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
	                    'title' => $image_title,
	                    'alt'   => $image_title,
	                    'data-zoom-image'=> $image_link
	                    ) );

	                $image_class = esc_attr( implode( ' ', $classes ) );
	        
	                echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s"   data-image="%s" class="%s" title="%s">%s</a>', $image_link, $image_link, $image_class, $image_caption, $image ), $attachment_id, $product->get_id(), $image_class );
	     
	                $loop++;
	            }
	            echo '</div>';
	        }     
	    ?>
	</div>
	<?php
	    if(count($attachment_ids)>$columns){
	    ?>
	    <!-- <div class="carousel-controls"> -->
	        <div class="carousel-controls carousel-controls-v3 hidden">
	            <a class="left carousel-control" href="#carousel-<?php echo esc_attr( $id ); ?>" data-slide="prev">
	                    <i class="fa fa-angle-left"></i>
	            </a>
	            <a class="right carousel-control" href="#carousel-<?php echo esc_attr( $id ); ?>" data-slide="next">
	                    <i class="fa fa-angle-right"></i>
	            </a>
	        </div>  
	    <!-- </div> -->
	    <?php } ?>


	</div>
	</div>
	    <?php
	}
}//end func

/**
 * Functions for getting parts of a price, in html, used by get_price_html.
 *
 * @param  string $from String or float to wrap with 'from' text
 * @param  mixed $to String or float to wrap with 'to' text
 * @return string
 */

function evolution_fnc_woocommerce_get_price_html_from_to( $price, $from, $to, $product) {
 $price = '<ins>' . ( ( is_numeric( $to ) ) ? wc_price( $to ) : $to ) . '</ins>'.'<del>' . ( ( is_numeric( $from ) ) ? wc_price( $from ) : $from ) . '</del>';
 return $price;
}
add_filter('woocommerce_get_price_html_from_to','evolution_fnc_woocommerce_get_price_html_from_to',10,4);



function evolution_fnc_woocommerce_load_media() {
    $prefixRTL = '';
    if (is_rtl()) {
        $prefixRTL = 'rtl-';
    }
	if(isset($_GET['opal-skin']) && $_GET['opal-skin']) {
		$current = $_GET['opal-skin'];
	}else{
		$current = str_replace( '.css','', evolution_fnc_theme_options('skin','default') );
	}
	
    if($current == 'default'){
        $path =  get_template_directory_uri() .'/css/'.$prefixRTL.'woocommerce.css';
    }else{
        $path =  get_template_directory_uri() .'/css/skins/'.$current.'/'.$prefixRTL.'woocommerce.css';
    }
    wp_enqueue_style( 'evolution-woocommerce', $path , 'evolution-woocommerce-front' , EVOLUTION_THEME_VERSION, 'all' );
}
add_action( 'wp_enqueue_scripts','evolution_fnc_woocommerce_load_media', 15 );

/**
 * Auto config product images after the theme actived.
 */
function evolution_fnc_woocommerce_setup(){
	$catalog = array(
		'width' 	=> '465',	// px
		'height'	=> '528',	// px
		'crop'		=> 1 		// true
	);

	$single = array(
		'width' 	=> '550',	// px
		'height'	=> '625',	// px
		'crop'		=> 1 		// true
	);

	$thumbnail = array(
		'width' 	=> '90',	// px
		'height'	=> '102',	// px
		'crop'		=> 1 		// true
	);

	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog );		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}

add_action( 'evolution_setup_theme_actived', 'evolution_fnc_woocommerce_setup');

/**
 */
add_filter('woocommerce_add_to_cart_fragments', 'evolution_fnc_woocommerce_header_add_to_cart_fragment' );

function evolution_fnc_woocommerce_header_add_to_cart_fragment( $fragments ){
	global $woocommerce;

 	$fragments['#cart .mini-cart-items'] =  sprintf(_n(' <span class="mini-cart-items">%d</span> ', ' <span class="mini-cart-items">%d</span> ', $woocommerce->cart->cart_contents_count, 'evolution'), $woocommerce->cart->cart_contents_count);
 	$fragments['#cart .mini-cart .amount'] = trim( $woocommerce->cart->get_cart_total() );
 	
    return $fragments;
}
add_filter( 'yith_wcwl_button_label',          'evolution_fnc_wpo_woocomerce_icon_wishlist'  );
add_filter( 'yith-wcwl-browse-wishlist-label', 'evolution_fnc_wpo_woocomerce_icon_wishlist_add' );

function evolution_fnc_wpo_woocomerce_icon_wishlist( $value='' ){
	return '<i class="fa fa-heart-o"></i><span>'.esc_html__('Add To Wishlist','evolution').'</span>';
}

function evolution_fnc_wpo_woocomerce_icon_wishlist_add(){
	return '<i class="fa fa-heart-o"></i><span>'.esc_html__('Add To Wishlist','evolution').'</span>';
}
/**
 * Mini Basket
 */
if(!function_exists('evolution_fnc_minibasket')){
    function evolution_fnc_minibasket( $style='' ){ 
        $template =  apply_filters( 'evolution_fnc_minibasket_template', evolution_fnc_get_header_layout( '' )  );
        if($template == 'default') $template = '';
 
        return get_template_part( 'woocommerce/cart/mini-cart-button', $template ); 
    }
}
if(evolution_fnc_theme_options("woo-show-minicart",true)){
	add_action( 'evolution_template_header_right', 'evolution_fnc_minibasket', 30, 0 );
}
/******************************************************
 * 												   	  *
 * Hook functions applying in archive, category page  *
 *												      *
 ******************************************************/
function evolution_template_woocommerce_main_container_class( $class ){ 
	if( is_product() ){
		$class .= ' '.  evolution_fnc_theme_options('woocommerce-single-layout') ;
	}else if( is_product_category() || is_archive()  ){ 
		$class .= ' '.  evolution_fnc_theme_options('woocommerce-archive-layout') ;
	}
	return $class;
}

add_filter( 'evolution_template_woocommerce_main_container_class', 'evolution_template_woocommerce_main_container_class' );
function evolution_fnc_get_woocommerce_sidebar_configs( $configs='' ){

	global $post; 
	$right = null; $left = null; 

	if( is_product() ){
		
		$left  =  evolution_fnc_theme_options( 'woocommerce-single-left-sidebar' ); 
		$right =  evolution_fnc_theme_options( 'woocommerce-single-right-sidebar' );  

	}else if( is_product_category() || is_archive() ){
		$left  =  evolution_fnc_theme_options( 'woocommerce-archive-left-sidebar' ); 
		$right =  evolution_fnc_theme_options( 'woocommerce-archive-right-sidebar' ); 
	}

 
	return evolution_fnc_get_layout_configs( $left, $right );
}

add_filter( 'evolution_fnc_get_woocommerce_sidebar_configs', 'evolution_fnc_get_woocommerce_sidebar_configs', 1, 1 );


function evolution_woocommerce_breadcrumb_defaults( $args ){
	$args['wrap_before'] = '<div class="opal-breadscrumb"><div class="container"><ol class="opal-woocommerce-breadcrumb breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>';
	$args['wrap_after'] = '</ol></div></div>';

	return $args;
}

add_filter( 'woocommerce_breadcrumb_defaults', 'evolution_woocommerce_breadcrumb_defaults' );

add_action( 'evolution_woo_template_main_before', 'woocommerce_breadcrumb', 30, 0 );
/**
 * Remove show page results which display on top left of listing products block.
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 10 );


function evolution_fnc_woocommerce_after_shop_loop_start(){
	echo '<div class="products-bottom-wrap clearfix">';
}

add_action( 'woocommerce_after_shop_loop', 'evolution_fnc_woocommerce_after_shop_loop_start', 1 );

function evolution_fnc_woocommerce_after_shop_loop_after(){
	echo '</div>';
}

add_action( 'woocommerce_after_shop_loop', 'evolution_fnc_woocommerce_after_shop_loop_after', 10000 );


/**
 * Wrapping all elements are wrapped inside Div Container which rendered in woocommerce_before_shop_loop hook
 */
function evolution_fnc_woocommerce_before_shop_loop_start(){
	echo '<div class="products-top-wrap clearfix">';
}

function evolution_fnc_woocommerce_before_shop_loop_end(){
	echo '</div>';
}


add_action( 'woocommerce_before_shop_loop', 'evolution_fnc_woocommerce_before_shop_loop_start' , 0 );
add_action( 'woocommerce_before_shop_loop', 'evolution_fnc_woocommerce_before_shop_loop_end' , 1000 );

function evolution_fnc_woocommerce_display_modes(){
	$woo_display = evolution_fnc_theme_options( 'wc_listgrid', 'grid' );
	if (isset($_GET['display'])){
		$woo_display = $_GET['display'];
	}
	echo '<form class="display-mode" method="get">';

		echo '<button title="'.esc_html__('Grid','evolution').'" class="btn '.($woo_display == 'grid' ? 'active' : '').'" value="grid" name="display" type="submit"><i class="fa fa-th"></i></button>'; 
		echo '<button title="'.esc_html__( 'List', 'evolution' ).'" class="btn '.($woo_display == 'list' ? 'active' : '').'" value="list" name="display" type="submit"><i class="fa fa-th-list"></i></button>'; 
	
		// Keep query string vars intact
		foreach ( $_GET as $key => $val ) {
			if ( 'display' === $key || 'submit' === $key ) {
				continue;
			}
			if ( is_array( $val ) ) {
				foreach( $val as $innerVal ) {
					echo '<input type="hidden" name="' . esc_attr( $key ) . '[]" value="' . esc_attr( $innerVal ) . '" />';
				}
			} else {
				echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $val ) . '" />';
			}
		}
	echo '</form>';

	}

add_action( 'woocommerce_before_shop_loop', 'evolution_fnc_woocommerce_display_modes' , 20 );


/**
 * Processing hook layout content
 */
function evolution_fnc_layout_main_class( $class ){
	$sidebar = evolution_fnc_theme_options( 'woo-sidebar-show', 1 ) ;
	if( is_single() ){
		$sidebar = evolution_fnc_theme_options('woo-single-sidebar-show'); ;
	}
	else {
		$sidebar = evolution_fnc_theme_options('woo-sidebar-show'); 
	}

	if( $sidebar ){
		return $class;
	}

	return 'col-lg-12 col-md-12 col-xs-12';
}
add_filter( 'evolution_woo_layout_main_class', 'evolution_fnc_layout_main_class', 4 );


/**
 *
 */
function evolution_fnc_woocommerce_archive_image(){ 
	if ( is_tax( array( 'product_cat', 'product_tag' ) ) && get_query_var( 'paged' ) == 0 ) { 
		$thumb =  get_woocommerce_term_meta( get_queried_object()->term_id, 'thumbnail_id', true ) ;

		if( $thumb ){
			$img = wp_get_attachment_image_src( $thumb, 'full' ); 
		
			echo '<p class="category-banner"><img src="'.$img[0].'""></p>'; 
		}
	}
}
add_action( 'woocommerce_archive_description', 'evolution_fnc_woocommerce_archive_image', 9 );


/**
 * Show/Hide related, upsells products
 */
function evolution_woocommerce_related_upsells_products($located, $template_name) {
	$options      = get_option('wpopal_theme_options');
	$content_none = get_template_directory() . '/woocommerce/content-none.php';

	if ( 'single-product/related.php' == $template_name ) {
		if ( isset( $options['wc_show_related'] ) && 
			( 1 == $options['wc_show_related'] ) ) {
			$located = $content_none;
		}
	} elseif ( 'single-product/up-sells.php' == $template_name ) {
		if ( isset( $options['wc_show_upsells'] ) && 
			( 1 == $options['wc_show_upsells'] ) ) {
			$located = $content_none;
		}
	}

	return apply_filters( 'evolution_woocommerce_related_upsells_products', $located, $template_name );
}

add_filter( 'wc_get_template', 'evolution_woocommerce_related_upsells_products', 10, 2 );

/**
 * Number of products per page
 */
function evolution_woocommerce_shop_per_page($number) {
	$value = isset($_GET['per_page']) ? $_GET['per_page'] : evolution_fnc_theme_options('woo-number-page', get_option('posts_per_page'));
	if ( is_numeric( $value ) && $value ) {
		$number = absint( $value );
	}
	return $number;
}

add_filter( 'loop_shop_per_page', 'evolution_woocommerce_shop_per_page' );

/**
 * Number of products per row
 */
function evolution_woocommerce_shop_columns($number) {
	$value = isset($_GET['column']) ? $_GET['column'] : evolution_fnc_theme_options('wc_itemsrow', 4);
	if ( in_array( $value, array(2, 3, 4, 6) ) ) {
		$number = $value;
	}
	return $number;
}

add_filter( 'loop_shop_columns', 'evolution_woocommerce_shop_columns' );

/**
 *
 */
function evolution_fnc_woocommerce_share_box() {
	if ( evolution_fnc_theme_options('wc_show_share_social', 1) ) {
		get_template_part( 'page-templates/parts/sharebox' );
	}
}
add_action( 'evolution_woocommerce_after_single_product_summary', 'evolution_fnc_woocommerce_share_box', 25 );

/**
 *
 */
function evolution_fnc_woo_product_nav(){
    echo '<div class="product-nav pull-right">';

        previous_post_link('<p>%link</p>', '<i class="fa fa-chevron-left"></i>', FALSE); 
        next_post_link('<p>%link</p>', '<i class="fa fa-chevron-right"></i>', FALSE); 

    echo '</div>';
}

add_action( 'evolution_woocommerce_after_single_product_title', 'evolution_fnc_woo_product_nav', 1 );


// rating star
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'evolution_woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating');

// remove woocommerce_cross_sell_display on page cart
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

//set stock
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

// WooCommerce Price
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25);

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 1 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
//add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
//add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );


//remove_action( 'evolution_woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
//add_action( 'evolution_woocommerce_single_product_summary', 'woocommerce_template_single_meta', 25 );

if( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
	require_once( get_template_directory() . '/inc/vendors/woocommerce/single-functions.php' );
}


/* ---------------------------------------------------------------------------
 * WooCommerce - Function get Query
 * --------------------------------------------------------------------------- */
 
function evolution_fnc_get_review_counting(){

    global $post; 
    $output = array();

    for($i=1; $i <= 5; $i++){
         $args = array(
            'post_id'      => ( $post->ID ),
            'meta_query' => array(
              array(
                'key'   => 'rating',
                'value' => $i
              )
            ),      
            'count' => true
        );
        $output[$i] = get_comments( $args );
    }
    return $output;
}

 
/* ---------------------------------------------------------------------------
 * WooCommerce - Function get Query
 * --------------------------------------------------------------------------- */
 


function evolution_fnc_woocommerce_before_shop_loop_item_title(){

    global $product;

    if( $product->get_regular_price() ){
        $percentage = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
        echo '<span class="product-sale-label">-' . trim( $percentage ) . '%</span>';
    }
                                            
}
 
/* ---------------------------------------------------------------------------
 * WooCommerce - Function Delete Produc Ttab
 * --------------------------------------------------------------------------- */
function delete_post_type(){
    unregister_post_type( 'producttab' );
}
add_action('init','delete_post_type');

/* ---------------------------------------------------------------------------
 * WooCommerce - Function 
 * --------------------------------------------------------------------------- */
add_filter('woocommerce_product_thumbnails_columns', 'evolution_fnc_product_image_thumnail');
function evolution_fnc_product_image_thumnail(){
 return evolution_fnc_theme_options('product-image-thumnail', 4);
}

/* ---------------------------------------------------------------------------
 * WooCommerce - availability 
 * --------------------------------------------------------------------------- */
add_filter( 'woocommerce_get_availability', 'custom_override_get_availability', 1, 2);
 
// The hook in function $availability is passed via the filter!
function custom_override_get_availability( $availability, $_product ) {
	if ( $_product->is_in_stock() )
		$availability['availability'] = esc_html__('In Stock', 'evolution');
	return $availability;
}

/*Remove description heading*/
add_filter( 'woocommerce_product_description_heading', 'opal_evolution_fnc_woocommerce_remove_product_description_heading' );
 function opal_evolution_fnc_woocommerce_remove_product_description_heading() {
  return '';
}

function evolution_fnc_woocommerce_price_html( $price,$product ){
   // return $product->price;
    if ( $product->get_price() > 0 ) {
      if ( $product->is_on_sale() ) {
        $from = $product->get_regular_price();
        $to = $product->get_price();
		// return '<ins>'.( ( is_numeric( $to ) ) ? wc_price( $to ) : $to ) .'</ins><del>'. ( ( is_numeric( $from ) ) ? wc_price( $from ) : $from ) .'</del>';
		return '<del>'. ( ( is_numeric( $from ) ) ? wc_price( $from ) : $from ) .'</del><ins style="margin-left: 4px !important">'.( ( is_numeric( $to ) ) ? wc_price( $to ) : $to ) .'</ins>';
      } else {
        $to = $product->get_price();
        return '<ins>' . ( ( is_numeric( $to ) ) ? wc_price( $to ) : $to ) . '</ins>';
      }
   }
}
add_filter( 'woocommerce_get_price_html', 'evolution_fnc_woocommerce_price_html', 100, 2 );
