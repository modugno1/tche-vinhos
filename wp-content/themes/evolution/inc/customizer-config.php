<?php
 /**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WpOpal Team <opalwordpress@gmail.com>
 * @copyright  Copyright (C) 2016 http://www.wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */

/**
 * Prestabrain Category Drop Down List Class
 * modified dropdown-pages from wp-includes/class-wp-customize-control.php
 *
 * @since Prestabrain v1.0
 */
function evolution_fnc_cst_customizer($wp_customize){
    $wp_customize->add_section('pbr_cst_general_settings', array(
        'title'      => esc_html__('General Settings', 'evolution'),
        'description'    => esc_html__('Website General Settings', 'evolution'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'sanitize_callback' => '__return_false',
    ));

    $wp_customize->add_setting('wpopal_theme_options[phone_number]', array(
        'default'    => '(00)-456 789 000',
        'type'       => 'option',
        'transport'=>'refresh',
        'sanitize_callback' => '__return_false',
    ) );

    $wp_customize->add_control( 'wpopal_theme_options[phone_number]', array(
        'label'    => esc_html__('Phone Number', 'evolution'),
        'section'  => 'pbr_cst_general_settings',
        'settings' => 'wpopal_theme_options[phone_number]',
        'priority' => 12,
        'sanitize_callback' => '__return_false',
    ) );


    $wp_customize->add_setting('wpopal_theme_options[topbar_email]', array(
        'default'    => 'support@evolution.com',
        'type'       => 'option',
        'transport'=>'refresh',
        'sanitize_callback' => 'sanitize_email',
        'sanitize_callback' => '__return_false',
    ) );

    $wp_customize->add_control( 'wpopal_theme_options[topbar_email]', array(
        'label'    => esc_html__('Email', 'evolution'),
        'section'  => 'pbr_cst_general_settings',
        'settings' => 'wpopal_theme_options[topbar_email]',
        'priority' => 12,
        'sanitize_callback' => '__return_false',
    ) ); 

    $wp_customize->add_setting('wpopal_theme_options[archive-show-title]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 0,
        'checked' => 1,
        'sanitize_callback' => '__return_false',
    ) );

    $wp_customize->add_control('wpopal_theme_options[archive-show-title]', array(
        'settings'  => 'wpopal_theme_options[archive-show-title]',
        'label'     => esc_html__('Enable Title', 'evolution'),
        'section'   => 'wc_archive_settings',
        'type'      => 'checkbox',
        'transport' => 4,
        'sanitize_callback' => '__return_false',
    ) );

    $wp_customize->add_setting('wpopal_theme_options[archive-show-banner]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 0,
        'checked' => 1,
        'sanitize_callback' => '__return_false',
    ) );

    $wp_customize->add_control('wpopal_theme_options[archive-show-banner]', array(
        'settings'  => 'wpopal_theme_options[archive-show-banner]',
        'label'     => esc_html__('Enable Banner', 'evolution'),
        'section'   => 'wc_archive_settings',
        'type'      => 'checkbox',
        'transport' => 4,
        'sanitize_callback' => '__return_false',
    ) );
    $wp_customize->add_setting('wpopal_theme_options[blog-show-title]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 0,
        'checked' => 1,
        'sanitize_callback' => '__return_false',
    ) );

    $wp_customize->add_control('wpopal_theme_options[blog-show-title]', array(
        'settings'  => 'wpopal_theme_options[blog-show-title]',
        'label'     => esc_html__('Enable title', 'evolution'),
        'section'   => 'archive_general_settings',
        'type'      => 'checkbox',
        'transport' => 4,
        'sanitize_callback' => '__return_false',
    ) );
    //Config zoom image 
    $wp_customize->add_setting( 'wpopal_theme_options[product-zoom-mode]', array(
        'type'       => 'option',
        'default'    => 'basic',
        'capability' => 'manage_options',
        'sanitize_callback' => '__return_false',
    ) );

    $wp_customize->add_control( 'wpopal_theme_options[product-zoom-mode]', array(
        'label'      => esc_html__( 'Product zoom mode', 'evolution' ),
        'section'    => 'wc_product_settings',
        
        'type'       => 'select',

        'choices'     => array(
            'basic' => esc_html__('Basic', 'evolution' ),
            'inner' => esc_html__('Inner', 'evolution' ),
        ), 
        'priority' => 3,
        'sanitize_callback' => '__return_false',
    ) );



    $wp_customize->add_setting('wpopal_theme_options[none-box-shadow]', array(
        'capability' => 'manage_options',
        'type'       => 'option',
        'default'   => 0,
        'checked' => 1,
        'sanitize_callback' => '__return_false',
    ) );

    $wp_customize->add_control('wpopal_theme_options[none-box-shadow]', array(
        'settings'  => 'wpopal_theme_options[none-box-shadow]',
        'label'     => esc_html__('Remove Box Shadow', 'evolution'),
        'section'  => 'pbr_cst_general_settings',
        'type'      => 'checkbox',
        'sanitize_callback' => '__return_false',
    ) );
}
add_action( 'customize_register', 'evolution_fnc_cst_customizer' );