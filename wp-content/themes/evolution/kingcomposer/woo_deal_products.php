<?php

/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WpOpal Team <opalwordpress@gmail.com>
 * @copyright  Copyright (C) 2016 http://www.wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */
    
    $atts  = array_merge( array(
        'number_post'  => 9,
        'columns'   => 4,
        'type'      => 'recent_products',
        'category'  => '',
        'subtitle'  => '',
        'layout'    => 'carousel',
        'style'     => '',
        'el_class'  => ''
    ), $atts); 

    extract( $atts );   

    $deals = array();
    $loop = wpopal_themer_woocommerce_query('deals', $number_post);
    $_id = wpopal_themer_makeid();
    $_count = 1;  
    switch ($columns) {
        case '5':
        case '4':
            $class_column='col-sm-6 col-md-3';
            $columns = 4; 
            break;
        case '3':
            $class_column='col-sm-4';
            break;
        case '2':
            $class_column='col-sm-6';
            break;
        default:
            $class_column='col-sm-12';
            break;
    }

    $_total =  $loop->found_posts;   

    if( $loop->have_posts()  ) {  ?> 
        <div class="woocommerce woo-deals <?php echo esc_attr($el_class) ?>">
        <?php if($layout == 'carousel'):?>
            <div id="carousel-<?php echo esc_attr($_id); ?>" class="owl-carousel-play products-collection" data-ride="owlcarousel">   
              
                <?php if( $_total > $columns ) {  ?>
                <div class="carousel-controls carousel-controls-v3 carousel-hidden hidden-xs">
                    <a class="left carousel-control" href="#carousel-<?php the_ID(); ?>" data-slide="prev">
                        <span class="fa fa-angle-left"></span><?php esc_html_e( 'previous deal', 'evolution' ); ?>
                    </a>
                    <a class="right carousel-control" href="#carousel-<?php the_ID(); ?>" data-slide="next">
                        <?php esc_html_e( 'next deal', 'evolution' ); ?><span class="fa fa-angle-right"></span>
                    </a>
                </div>
                <?php } ?>
                 <div class="owl-carousel rows-products" data-slide="<?php echo esc_attr($columns); ?>" data-pagination="false" data-navigation="true">
                    <?php 
                        $style = ($style=='grid') ? 'style-grid' : 'list';
                         while ( $loop->have_posts() ) : $loop->the_post();  
                            $product = wc_get_product();
                            $term_list = wp_get_post_terms($product->get_id(),'product_cat');
                            $time_sale = get_post_meta( $product->get_id(), '_sale_price_dates_to', true );
                            

                    ?>
            
                            <div class="product product-<?php echo esc_attr($style);?> <?php if($_count%$columns==0) echo ' last'; ?>">
                                <div class="product-block" data-product-id="<?php echo esc_attr($product->get_id()); ?>">
                                    <figure class="image">
                                        <?php woocommerce_show_product_loop_sale_flash(); ?>
                                        <a title="<?php the_title(); ?>" href="<?php echo (get_option( 'woocommerce_enable_lightbox' )=='yes' && is_product()) ? $image_attributes[0] : the_permalink(); ?>" class="product-image <?php echo (get_option( 'woocommerce_enable_lightbox' )=='yes' &&  is_product())?'zoom':'zoom-2' ;?>">
                                            <?php
                                                /**
                                                * woocommerce_before_shop_loop_item_title hook
                                                *
                                                * @hooked woocommerce_show_product_loop_sale_flash - 10
                                                * @hooked woocommerce_template_loop_product_thumbnail - 10
                                                */
                                                remove_action('woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash', 10);
                                                do_action( 'woocommerce_before_shop_loop_item_title' );
                                            ?>
                                        </a> 
                                       <div class="button-action clearfix"> 
                                            <div class="button-groups">

                                                <?php
                                                    if( class_exists( 'YITH_WCWL' ) ) {
                                                        echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
                                                    }
                                                ?>

                                                <?php if( class_exists( 'YITH_Woocompare' ) ) { ?>
                                                    <?php
                                                        $action_add = 'yith-woocompare-add-product';
                                                        $url_args = array(
                                                            'action' => $action_add,
                                                            'id' => $product->get_id()
                                                        );
                                                    ?>
                                                    <div class="yith-compare">
                                                        <a title="<?php esc_html_e( 'Add to compare', 'evolution' ); ?>" href="<?php echo wp_nonce_url( add_query_arg( $url_args ), $action_add ); ?>" class="compare" data-product_id="<?php echo esc_attr($product->get_id()); ?>">
                                                            <em class="fa fa-refresh"></em>
                                                        </a>
                                                    </div>
                                                <?php } ?>

                                                <?php if(evolution_fnc_theme_options('is-quickview', true)){ ?>
                                                    <div class="quick-view hidden-xs hidden-sm">
                                                        <a title="<?php esc_html_e( 'Quick view', 'evolution' ); ?>" href="#" class="quickview" data-productslug="<?php echo trim($product->get_slug()); ?>" data-toggle="modal" data-target="#opal-quickview-modal">
                                                           <i class="fa fa-eye"> </i><span><?php esc_html_e( 'Quick view', 'evolution' ); ?></span>
                                                        </a>
                                                    </div>
                                                <?php } ?> 

                                            </div>
                                        </div>          
                                    </figure>

                                    <div class="caption">
                                        <div class="meta">
                                            <?php if($term_list && isset($term_list[0]) && !is_product_category() ):?>
                                                <div class="category-name">
                                                    <a href="<?php echo esc_url(get_term_link ($term_list[0]->term_id, 'product_cat'));?>"><?php echo trim($term_list[0]->name); ?></a>
                                                </div>
                                            <?php endif; ?>
                                            <h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                                        </div>
                                        
                                        <?php
                                                do_action( 'woocommerce_after_shop_loop_item_title');
                                            ?>
                                        <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
                                        <?php
                                            $action_add = 'yith-woocompare-add-product';
                                            $url_args = array(
                                                'action' => $action_add,
                                                'id' => $product->get_id()
                                            );
                                        ?> 
                                    </div>
                                    <div class="time">
                                        <?php if( $time_sale ) { ?>
                                            <span><?php esc_html_e( 'hurry up! Offer Ends in:', 'evolution' ); ?></span>
                                            <div class="pts-countdown clearfix" data-countdown="countdown" data-days="<?php esc_html_e('Days', 'evolution');?>" data-hours="<?php esc_html_e('Hours', 'evolution');?>" data-minutes="<?php esc_html_e('Mins', 'evolution');?>" data-seconds="<?php esc_html_e('Secs', 'evolution');?>"
                                                         data-date="<?php echo date('m',$time_sale).'-'.date('d',$time_sale).'-'.date('Y',$time_sale).'-'. date('H',$time_sale) . '-' . date('i',$time_sale) . '-' .  date('s',$time_sale) ; ?>">
                                                    </div>
                                        <?php } ?>
                                    </div> 
                                    
                                </div>
                            </div>
                         
                <?php 
                        $_count++; 
                    endwhile; 
                ?>
               <?php wp_reset_postdata(); ?>
                </div>
            </div>
            <?php elseif($layout == 'grid') : ?>
                 <div class="widget_products" id="<?php echo esc_attr($_id); ?>">
                    <div class="products-<?php echo esc_attr($style);?>">
                        <?php   
                            $style = ($style=='grid') ? 'style-grid' : 'list';  
                            while ( $loop->have_posts() ) : $loop->the_post(); 

                            $product = wc_get_product();   
                            $term_list = wp_get_post_terms($product->get_id(),'product_cat');
                            $time_sale = get_post_meta( $product->get_id(), '_sale_price_dates_to', true );  

                        ?>
                        <?php if( $_count%$columns == 1 || $columns == 1 ) echo '<div class="item'.(($_count==1)?" active":"").'"><div class="row-products row">'; ?>
                       
                                <div class="product-wrapper product-<?php echo esc_attr($style);?> product <?php echo esc_attr( $class_column ); if($_count%$columns==0) echo ' last'; ?>">
                                    <div class="product-block" data-product-id="<?php echo esc_attr($product->get_id()); ?>">
                                        <figure class="image">
                                            <?php woocommerce_show_product_loop_sale_flash(); ?>
                                            <a title="<?php the_title(); ?>" href="<?php echo (get_option( 'woocommerce_enable_lightbox' )=='yes' && is_product()) ? $image_attributes[0] : the_permalink(); ?>" class="product-image <?php echo (get_option( 'woocommerce_enable_lightbox' )=='yes' &&  is_product())?'zoom':'zoom-2' ;?>">
                                                <?php
                                                    /**
                                                    * woocommerce_before_shop_loop_item_title hook
                                                    *
                                                    * @hooked woocommerce_show_product_loop_sale_flash - 10
                                                    * @hooked woocommerce_template_loop_product_thumbnail - 10
                                                    */
                                                    remove_action('woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash', 10);
                                                    do_action( 'woocommerce_before_shop_loop_item_title' );
                                                ?>
                                            </a> 
                                           <div class="button-action clearfix"> 
                                                <div class="button-groups">

                                                    <?php
                                                        if( class_exists( 'YITH_WCWL' ) ) {
                                                            echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
                                                        }
                                                    ?>

                                                    <?php if( class_exists( 'YITH_Woocompare' ) ) { ?>
                                                        <?php
                                                            $action_add = 'yith-woocompare-add-product';
                                                            $url_args = array(
                                                                'action' => $action_add,
                                                                'id' => $product->get_id()
                                                            );
                                                        ?>
                                                        <div class="yith-compare">
                                                            <a title="<?php esc_html_e( 'Add to compare', 'evolution' ); ?>" href="<?php echo wp_nonce_url( add_query_arg( $url_args ), $action_add ); ?>" class="compare" data-product_id="<?php echo esc_attr($product->get_id()); ?>">
                                                                <em class="fa fa-refresh"></em>
                                                            </a>
                                                        </div>
                                                    <?php } ?>

                                                    <?php if(evolution_fnc_theme_options('is-quickview', true)){ ?>
                                                        <div class="quick-view hidden-xs hidden-sm">
                                                            <a title="<?php esc_html_e( 'Quick view', 'evolution' ); ?>" href="#" class="quickview" data-productslug="<?php echo trim($product->get_slug()); ?>" data-toggle="modal" data-target="#opal-quickview-modal">
                                                               <i class="fa fa-eye"> </i><span><?php esc_html_e( 'Quick view', 'evolution' ); ?></span>
                                                            </a>
                                                        </div>
                                                    <?php } ?> 

                                                </div>
                                            </div>          
                                        </figure>

                                        <div class="caption">
                                            <div class="meta">
                                                <?php if($term_list && isset($term_list[0]) && !is_product_category() ):?>
                                                    <div class="category-name">
                                                        <a href="<?php echo esc_url(get_term_link ($term_list[0]->term_id, 'product_cat'));?>"><?php echo trim($term_list[0]->name); ?></a>
                                                    </div>
                                                <?php endif; ?>
                                                <h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                                            </div>

                                            <?php
                                                    do_action( 'woocommerce_after_shop_loop_item_title');
                                                ?>
                                            <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
                                            <?php
                                                $action_add = 'yith-woocompare-add-product';
                                                $url_args = array(
                                                    'action' => $action_add,
                                                    'id' => $product->get_id()
                                                );
                                            ?>               
                                        </div>
                                        <div class="time">
                                            <?php if( $time_sale ) { ?>
                                                <span><?php esc_html_e( 'hurry up! Offer Ends in:', 'evolution' ); ?></span>
                                                <div class="pts-countdown clearfix" data-countdown="countdown" data-days="<?php esc_html_e('Days', 'evolution');?>" data-hours="<?php esc_html_e('Hours', 'evolution');?>" data-minutes="<?php esc_html_e('Mins', 'evolution');?>" data-seconds="<?php esc_html_e('Secs', 'evolution');?>"
                                                         data-date="<?php echo date('m',$time_sale).'-'.date('d',$time_sale).'-'.date('Y',$time_sale).'-'. date('H',$time_sale) . '-' . date('i',$time_sale) . '-' .  date('s',$time_sale) ; ?>">
                                                    </div>
                                            <?php } ?>
                                        </div> 
                                        
                                    </div>

                                </div>
                            <?php if( ($_count%$columns==0 && $_count!=1) || $_count== $_total || $columns ==1 ) echo '</div></div>'; ?>
                    <?php 
                            $_count++; 
                        endwhile; 
                    ?>
                    <?php wp_reset_postdata(); ?>
                    </div>
                </div>     

            <?php endif ?>    
        </div>
 
    <?php } ?>

     
