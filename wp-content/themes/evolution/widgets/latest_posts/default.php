<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WordPress Opal Team <opalwordpress@gmail.com >
 * @copyright  Copyright (C) 2015 prestabrain.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */
// Display the widget title
if ( $title ) {
    echo ($before_title)  . trim( $title ) . $after_title;
}

$query = new WP_Query( array( 'posts_per_page' => $instance[ 'number_post' ] ) );
$_count = 0;


	


if($query->have_posts()){
?>
	<div class="blog-post">
	<?php while ( $query->have_posts() ): $query->the_post(); ?>
		<?php if($_count%$instance[ 'number_rows' ] ==0): ?>
				<div class="item">
		<?php endif; ?>
			<div class="content-blog">
				<div class="image-thumnail">
					<?php evolution_fnc_post_thumbnail(); ?>
				</div>

				<?php 
				the_title( '<h3 class="title-post"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
				?>
				<div class="bottom-blog">
					<div class="entry-date pull-left">
				        <span><?php the_time( 'd' ); ?></span>&nbsp;<?php the_time( 'M' ); ?>
				   </div>
				   <div class="author pull-right"><i class="fa fa-pencil" aria-hidden="true"></i><?php esc_html_e('Post by:', 'evolution');?></i><?php the_author_posts_link(); ?></div>
				</div>
				<div class="entry-content">
					<?php
					/* translators: %s: Name of current post */
					echo evolution_fnc_excerpt( 15, '...' );
					?>
				</div><!-- .entry-content -->
			</div>
		<?php if($_count%$instance[ 'number_rows' ] == $instance[ 'number_rows' ]-1 || $_count==$query->post_count()-1): ?>
				</div>
		<?php endif; ?>
		<?php $_count++; ?>
	<?php endwhile; ?>
	</div>
<?php } ?>
<?php wp_reset_postdata(); ?>