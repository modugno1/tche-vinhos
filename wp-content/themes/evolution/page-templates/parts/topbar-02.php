<section id="opal-topbar" class="opal-topbar style-2 hidden-xs hidden-sm">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 info">
                <?php if ( is_active_sidebar( 'widget-topbar' ) ) : ?>
                    <?php dynamic_sidebar( 'widget-topbar' ); ?>
                <?php endif; ?>
            </div> 
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                
                <div class="hidden-xs hidden-sm">
                    
                   <ul class="list-style  pull-right">
                        <li class="fa fa-pencil-square-o icon space-right-13" aria-hidden="true"></li>

                        <?php if( !is_user_logged_in() ){ ?>
                            <?php
                            do_action( 'opal-account-buttons' );
                        ?>
                        <?php }else{ ?>
                            <?php $current_user = wp_get_current_user(); ?>
                          <li>  <span class="hidden-xs"><?php esc_html_e('Welcome ','evolution'); ?><?php echo esc_html( $current_user->display_name); ?> !</span></li>
                        <?php } ?>
                        <li></li>
                    </ul>

                </div>
            </div>                                 
        </div>
    </div> 
</section>