<?php
/**
 * mode Customizer support
 *
 * @package WpOpal
 * @subpackage evolution
 * @since evolution 1.0
 */

if ( ! function_exists( 'evolution_fnc_customize_register' ) ) :
function evolution_fnc_customize_register($wp_customize){
    $wp_customize->remove_section('colors');

    // Add Panel Colors
    $wp_customize->add_panel('colors', array(
        'priority' => 15,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => esc_html__('Colors', 'evolution'),
    ));
    /* OpalTool: inject code */
    
    // Primary Color
    $wp_customize->add_section('evolution_main_color', array(
        'title'      => esc_html__('Main Color', 'evolution'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));
    $wp_customize->add_setting('evolution_maincolor_primary_color', array(
        'default'    => get_option('evolution_maincolor_primary_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control header_bg
    $wp_customize->add_control('evolution_maincolor_primary_color', array(
        'label'    => esc_html__('Primary Color', 'evolution'),
        'section'  => 'evolution_main_color',
        'type'      => 'color',
    ) );/* OpalTool: end inject code */
        // Add Section header
    $wp_customize->add_section('evolution_color_header', array(
        'title'      => esc_html__('Header', 'evolution'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting topbar_bg
    $wp_customize->add_setting('evolution_color_topbar_bg', array(
        'default'    => get_option('evolution_color_topbar_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control topbar_bg
    $wp_customize->add_control('evolution_color_topbar_bg', array(
        'label'    => esc_html__(' Topbar Background', 'evolution'),
        'section'  => 'evolution_color_header',
        'type'      => 'color',
    ) );
    // Add setting topbar_color
    $wp_customize->add_setting('evolution_color_topbar_color', array(
        'default'    => get_option('evolution_color_topbar_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control topbar_color
    $wp_customize->add_control('evolution_color_topbar_color', array(
        'label'    => esc_html__('Topbar color', 'evolution'),
        'section'  => 'evolution_color_header',
        'type'      => 'color',
    ) );
    // Add setting header_bg
    $wp_customize->add_setting('evolution_color_header_bg', array(
        'default'    => get_option('evolution_color_header_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control header_bg
    $wp_customize->add_control('evolution_color_header_bg', array(
        'label'    => esc_html__('Header Background', 'evolution'),
        'section'  => 'evolution_color_header',
        'type'      => 'color',
    ) );
    // Add setting menu_color
    $wp_customize->add_setting('evolution_color_menu_color', array(
        'default'    => get_option('evolution_color_menu_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control menu_color
    $wp_customize->add_control('evolution_color_menu_color', array(
        'label'    => esc_html__('Menu color', 'evolution'),
        'section'  => 'evolution_color_header',
        'type'      => 'color',
    ) );
    // Add setting menu_active_hover_color
    $wp_customize->add_setting('evolution_color_menu_active_hover_color', array(
        'default'    => get_option('evolution_color_menu_active_hover_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control menu_active_hover_color
    $wp_customize->add_control('evolution_color_menu_active_hover_color', array(
        'label'    => esc_html__('Menu Active, Hover color', 'evolution'),
        'section'  => 'evolution_color_header',
        'type'      => 'color',
    ) );
    // Add Section footer
    $wp_customize->add_section('evolution_color_footer', array(
        'title'      => esc_html__('Footer', 'evolution'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting copyright_bg
    $wp_customize->add_setting('evolution_color_copyright_bg', array(
        'default'    => get_option('evolution_color_copyright_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control copyright_bg
    $wp_customize->add_control('evolution_color_copyright_bg', array(
        'label'    => esc_html__('Footer Copyright Background', 'evolution'),
        'section'  => 'evolution_color_footer',
        'type'      => 'color',
    ) );
    // Add setting copyright_color
    $wp_customize->add_setting('evolution_color_copyright_color', array(
        'default'    => get_option('evolution_color_copyright_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control copyright_color
    $wp_customize->add_control('evolution_color_copyright_color', array(
        'label'    => esc_html__('Copyright Color', 'evolution'),
        'section'  => 'evolution_color_footer',
        'type'      => 'color',
    ) );

}
endif;
add_action('customize_register', 'evolution_fnc_customize_register', 99);