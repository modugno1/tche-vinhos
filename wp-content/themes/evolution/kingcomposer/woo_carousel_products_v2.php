<?php 
	$type = 'recent_products';
	$number_post = 10;
	$number_per_post = 4;
	$category = '';
	extract( $atts );
	$_id = time().rand();
	$columns = $number_per_post-1;
	if( isset($woocategory) && !empty($woocategory) ){
		$category = array_keys( wpopal_themer_autocomplete_options_helper($woocategory) );
	} 
	$loop = wpopal_themer_woocommerce_query( $type, $number_post , $category  );
	$_count = 0;
	$_num = 0;
?>
<div class="clearfix"></div>	
<div class="products-collection <?php echo esc_attr($style);?> owl-carousel-play woocommerce carousel-products <?php echo esc_attr($position); ?>" id="postcarousel-<?php echo esc_attr($_id); ?>" data-ride="carousel" >

	<?php if( $loop->post_count > $columns+1 ) { ?>
		<div class="carousel-controls carousel-controls-v2 carousel-hidden hidden-xs">
			<a href="#postcarousel-<?php echo esc_attr($_id); ?>" data-slide="prev" class="left carousel-control  carousel-md">
				<span class="fa fa-angle-left"></span>
			</a>
			<a href="#postcarousel-<?php echo esc_attr($_id); ?>" data-slide="next" class="right carousel-control  carousel-md">
				<span class="fa fa-angle-right"></span>
			</a>
		</div>
	<?php } ?>
	<div class="owl-carousel" data-slide="1"  data-singleItem="false" data-navigation="false" data-pagination="false">
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			
			<?php if($_count==0): ?>
				<div class="item">
					<div class="product-carousel-item post-11943 product type-product status-publish has-post-thumbnail product_cat-accessories product_cat-cameras first instock featured shipping-taxable purchasable product-type-simple main-product">
						<?php wc_get_template_part( 'content', 'product-list-v2' ); ?>
					</div>
					<ul class="list-product">
				<?php else: ?>
					<?php wc_get_template_part( 'content', 'product-list' );?>
				<?php endif; ?>
			<?php if( $_count == $columns || $_num == $loop->post_count - 1 ): ?>
					</ul><!-- /.col -->
				</div><!-- /.item -->
			<?php endif; ?>
			<?php if($_count == $columns){ 
					$_count = 0;
				}else{
					$_count++ ;
				}
				$_num ++;?>
		<?php endwhile; ?>

	</div>
</div>

<?php wp_reset_postdata(); ?>