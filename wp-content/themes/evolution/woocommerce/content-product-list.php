<?php $product = wc_get_product(); ?>

<?php
	$class=$attrs='';
	if(isset($animate) && $animate){
		$class= 'wow fadeInUp';
		//$attrs = 'data-wow-duration="0.6s" data-wow-delay="'.$delay.'ms"';
	}
?>

<li class="media product-block widget-product <?php echo esc_attr($class); ?>" <?php echo trim($attrs); ?>>
	<?php if((isset($item_order) && $item_order==1) || !isset($item_order)) : ?>
		<a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>" class="image">
			<?php echo trim( $product->get_image() ); ?>
			<?php if(isset($item_order) && $item_order==1) { ?> 
				<span class="first-order"><?php echo trim($item_order); ?></span>
			<?php } ?>
		</a>
	<?php endif; ?>
	<?php if(isset($item_order) && $item_order > 1){ ?>
		<div class="order"><span><?php echo trim($item_order); ?></span></div>
	<?php }?>
	<div class="meta">
	    <h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>		 
		 <div class="caption">

         	<?php
                 do_action( 'woocommerce_after_shop_loop_item_title');
             ?>

      		<?php do_action( 'evolution_woocommerce_before_shop_loop_item_title'); ?>
                
        </div>
	</div>
</li>


