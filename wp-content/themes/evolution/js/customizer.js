( function( $ ) {
    // evolution_color_topbar_bg
    wp.customize( 'evolution_color_topbar_bg', function( value ) {
        value.bind( function( newval ) {
            $('#evolution-topbar_bg-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="evolution-topbar_bg-js-customizer">#opal-topbar{background-color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // evolution_color_topbar_color
    wp.customize( 'evolution_color_topbar_color', function( value ) {
        value.bind( function( newval ) {
            $('#evolution-topbar_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="evolution-topbar_color-js-customizer">#opal-topbar,#opal-topbar a {color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // evolution_color_header_bg
    wp.customize( 'evolution_color_header_bg', function( value ) {
        value.bind( function( newval ) {
            $('#evolution-header_bg-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="evolution-header_bg-js-customizer">#opal-masthead{background-color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // evolution_color_menu_color
    wp.customize( 'evolution_color_menu_color', function( value ) {
        value.bind( function( newval ) {
            $('#evolution-menu_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="evolution-menu_color-js-customizer">.navbar-mega .navbar-nav > li > a{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // evolution_color_menu_active_hover_color
    wp.customize( 'evolution_color_menu_active_hover_color', function( value ) {
        value.bind( function( newval ) {
            $('#evolution-menu_active_hover_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="evolution-menu_active_hover_color-js-customizer">.navbar-mega .navbar-nav > li:hover > a, .navbar-mega .navbar-nav > li:focus > a,.navbar-mega .navbar-nav > li.active > a{color:'+newval.toString()+';}.navbar-mega .navbar-nav > li.active > a:before, .navbar-mega .navbar-nav > li:hover > a:before, .navbar-mega .navbar-nav > li:focus > a:before{background-color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // evolution_color_copyright_bg
    wp.customize( 'evolution_color_copyright_bg', function( value ) {
        value.bind( function( newval ) {
            $('#evolution-copyright_bg-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="evolution-copyright_bg-js-customizer">.opal-copyright{background-color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // evolution_color_copyright_color
    wp.customize( 'evolution_color_copyright_color', function( value ) {
        value.bind( function( newval ) {
            $('#evolution-copyright_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="evolution-copyright_color-js-customizer">.opal-copyright{color:'+newval.toString()+';}</style>');
            }
        } );
    } );

    /* OpalTool: inject code */
    
    wp.customize('evolution_maincolor_primary_color', function(value){
		value.bind(function(to){
		    if(to == ''){
		        var style = $('#evolution_maincolor_primary_color-header-css'),
                color = style.data('color'),
                css = style.html();
                css = css.split(color).join('__maincolor__');
                style.html(css).data('color', '__maincolor__');
		    }else{
                var style = $('#evolution_maincolor_primary_color-header-css'),
                color = style.data('color'),
                css = style.html();
                css = css.split(color).join(to);
                style.html(css).data('color', to);
            }
		});
	});/* OpalTool: end inject code */
} )( jQuery );