<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WpOpal
 * @subpackage evolution
 * @since evolution 1.0
 */
/*
*Template Name: 404 Page
*/

get_header( apply_filters( 'evolution_fnc_get_header_layout', null ) ); ?>

<section id="main-container" class="<?php echo apply_filters('evolution_template_main_container_class','container');?> inner clearfix notfound-page">
	<div class="row">
		<div id="main-content" class="main-content">
			<div id="primary" class="content-area">
				 <div id="content" class="site-content" role="main">
				 	<div class="container">
				 		<div class="bg-page-404">
				 			<div class="col-lg-12 col-md-12 col-sm-12 left-notfound text-center">
								<div class="title">
									<span>404</span>							
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 text-center">
								<span class="sub"><?php esc_html_e( 'Page Not Found !', 'evolution' ); ?></span>
								<div class="error-description">
									<p><?php esc_html_e( 'We are sorry, but something went wrong', 'evolution' ); ?></p>
								</div><!-- .page-content -->	
							</div>
							<div class="page-action col-lg-12 col-md-12 col-sm-12 text-center">
								<a class="btn btn-sm btn-primary" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Return to homepage', 'evolution'); ?></a>
							</div>
				 		</div>
				 	</div>
				</div><!-- #content -->
			</div><!-- #primary -->
			<?php get_sidebar( 'content' ); ?>
		</div><!-- #main-content -->

		 
		<?php get_sidebar(); ?>
	 
	</div>	
</section>
<?php

get_footer();

 