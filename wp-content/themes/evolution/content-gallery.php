<?php
/**
 * The template for displaying posts in the Gallery post format
 *
 * @package WpOpal
 * @subpackage evolution
 * @since evolution 1.0
 */

$galleries = evolution_fnc_get_post_galleries();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-preview">
		<?php if( $galleries ): ?>
		<div id="post-slide-<?php the_ID(); ?>" class="owl-carousel-play" data-ride="carousel">
			<div class="owl-carousel" data-slide="1"  data-singleItem="true" data-navigation="true" data-pagination="false">
				<?php foreach ($galleries as $key => $_img) {
					echo '<img src="'.$_img.'" alt="">';
				} ?>
			</div>
			<a class="left carousel-control carousel-xs radius-x" data-slide="prev" href="#post-slide-<?php the_ID(); ?>">
				<span class="fa fa-angle-left"></span>
			</a>
			<a class="right carousel-control carousel-xs radius-x" data-slide="next" href="#post-slide-<?php the_ID(); ?>">
				<span class="fa fa-angle-right"></span>
			</a>
		</div>
		<?php else : ?>	
		<?php evolution_fnc_post_thumbnail(); ?>
		<?php endif; ?>
		
		<span class="post-format">
			<a class="entry-format" href="<?php echo esc_url( get_post_format_link( 'image' ) ); ?>"><i class="fa fa-th-large"></i></a>
		</span>
	</div>

	<header class="entry-header">
		<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && evolution_fnc_categorized_blog() ) : ?>
		<div class="entry-meta">
			<span class="cat-links"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'evolution' ) ); ?></span>
		</div><!-- .entry-meta -->
		<?php
			endif;

			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			endif;
		?>

		<div class="entry-meta">
			
			<div class="entry-date">
            <span><?php the_time( 'd' ); ?></span>&nbsp;<?php the_time( 'M' ); ?>
         </div>
         <div class="author"><?php esc_html_e('by', 'evolution'); the_author_posts_link(); ?></div>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			if(is_single()){
				the_content( sprintf(
					esc_html__( 'Continue reading %s', 'evolution').'<span class="meta-nav">&rarr;</span>',
					the_title( '<span class="screen-reader-text">', '</span>', false )
				) );
			}else{
				the_excerpt();
			}

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'evolution' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
</article><!-- #post-## -->
