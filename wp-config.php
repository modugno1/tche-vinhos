<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vinho');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

// permission
define('FS_METHOD', 'direct');

define('FS_CHMOD_DIR', (0755 & ~ umask()));
define('FS_CHMOD_FILE', (0644 & ~ umask()));

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'O(u5/%{W*FN4}g[LgdJOTVMr,CP8QFsBTN%evt1Vnhhu@3Wv{y,FNXulz*;Kbj9u');
define('SECURE_AUTH_KEY',  ';rd??u&Rt4@})?pK|GI@{G78HZ^*by,k,2nuW_NK: 1Rs%Y<GL,^,M{I3)dTwig>');
define('LOGGED_IN_KEY',    't*rMhz/0=3arj3J.xuvwW%OEF)5/uQGmj}BF9-g[g,isgY%3}a>PFnHJNbH>/WWf');
define('NONCE_KEY',        'nyj%ar6<Fk|UsSUg74>VOh%Oa:uZXE zA~)wQykQj1,HqTSm}_TjyJ]EtBI;H$`Q');
define('AUTH_SALT',        'x58`e#%l%top,fF6 Vm&LBVfcf]I%$c!c^NC&` f{nPXAR7R23{ONB|rLScDObTg');
define('SECURE_AUTH_SALT', 'l=wnWEkF:Z2bpJOZ/f&$F^gk-%5fYCQL9b{|d 5FJ:)y;(#Il]y]PlhW.0<^xl4.');
define('LOGGED_IN_SALT',   's<X1;H) {{Ue^+p29T4K[|^9<O$I|54y1^|~Sr9|g{Zt$lE>zfLYj:>Gld)FcV<+');
define('NONCE_SALT',       'J74tiVKWTgEu48<o[-wx9EbEw|gD;f=o<8#{Jb{]W3jsq8hqQ&Cwc&zJ)kr}8wVy');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WPLANG', 'pt_BR');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
